/*
 * debug.h
 *
 *  Created on: Oct 19, 2021
 *      Author: admin
 */

#ifndef INC_DEBUG_H_
#define INC_DEBUG_H_

#include "main.h"
#include "debugprint.h"
#include "stdint.h"
#include "string.h"


#define PRODUCT "SOLINK"
#define DBGU_RX_LOW_BUF_MAX				(1024 * 1)
#define DBGU_RX_FRAME_BUF_MAX			500
#define DBGU_TX_DMA_BUF_MAX				2000
#define DBGU_TX_BUF_MAX					2000
#define M_SIZE_BUF_TROUGH 2000

typedef struct
{
	int32_t buf_size;
	int32_t buf_ready;
	int32_t head_cur;
	int32_t head_lat;
	int32_t tail_cur;
	int32_t last_rx_counting;
	uint8_t * buf;
}_UART_BUF;

typedef struct
{
	int32_t h;//head;
	int32_t t;//tail;
	uint8_t buf[M_SIZE_BUF_TROUGH];
	uint8_t c_prev;
}_buf_scc;


typedef struct _SYSTEM_ADC
{
	int32_t celsius;
	int32_t value;
	int32_t kalman;
	int32_t prev_value;
	int32_t error;
}SYSTEM_ADC;



typedef struct _SYSTEM_INFO
{
	int32_t obc;
	int32_t TxMODEM;
	int32_t RxMODEM;
	SYSTEM_ADC adc[2];
	int32_t gpio[8];
	int32_t runningTime;				//sec.
	int32_t pps;				//sec.
	int32_t ppm;				//sec.

}SYSTEM_INFO;

extern SYSTEM_INFO systeminfo;

uint32_t debugBufferInit(_UART_BUF *buf);
void debugInit(void);
void ShowPrompt(void);
void ShowStatus(void);
void dbgu_rx_low_recv(int8_t c);
void debug_rx_frame_handle(void);

#endif /* INC_DEBUG_H_ */
