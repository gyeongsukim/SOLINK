/*
 * heap.h
 *
 *  Created on: 2019. 1. 23.
 *      Author: HOBEOM
 */

#ifndef HEAP_H_
#define HEAP_H_

void Init_heap();
void heap_push(int element);
int heap_pop();



#endif /* HEAP_H_ */
