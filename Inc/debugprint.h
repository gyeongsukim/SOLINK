/*
 * debugprint.h
 *
 *  Created on: Oct 19, 2021
 *      Author: admin
 */

#ifndef INC_DEBUGPRINT_H_
#define INC_DEBUGPRINT_H_

#include "stdint.h"


#define BLK "\e[0;30m"
#define RED "\e[0;31m"
#define GRN "\e[0;32m"
#define YEL "\e[0;33m"
#define BLU "\e[0;34m"
#define MAG "\e[0;35m"
#define CYN "\e[0;36m"
#define WHT "\e[0;37m"

//Regular bold text
#define BBLK "\e[1;30m"
#define BRED "\e[1;31m"
#define BGRN "\e[1;32m"
#define BYEL "\e[1;33m"
#define BBLU "\e[1;34m"
#define BMAG "\e[1;35m"
#define BCYN "\e[1;36m"
#define BWHT "\e[1;37m"

//Regular underline text
#define UBLK "\e[4;30m"
#define URED "\e[4;31m"
#define UGRN "\e[4;32m"
#define UYEL "\e[4;33m"
#define UBLU "\e[4;34m"
#define UMAG "\e[4;35m"
#define UCYN "\e[4;36m"
#define UWHT "\e[4;37m"

//Regular background
#define BLKB "\e[40m"
#define REDB "\e[41m"
#define GRNB "\e[42m"
#define YELB "\e[43m"
#define BLUB "\e[44m"
#define MAGB "\e[45m"
#define CYNB "\e[46m"
#define WHTB "\e[47m"

//High intensty background
#define BLKHB "\e[0;100m"
#define REDHB "\e[0;101m"
#define GRNHB "\e[0;102m"
#define YELHB "\e[0;103m"
#define BLUHB "\e[0;104m"
#define MAGHB "\e[0;105m"
#define CYNHB "\e[0;106m"
#define WHTHB "\e[0;107m"

//High intensty text
#define HBLK "\e[0;90m"
#define HRED "\e[0;91m"
#define HGRN "\e[0;92m"
#define HYEL "\e[0;93m"
#define HBLU "\e[0;94m"
#define HMAG "\e[0;95m"
#define HCYN "\e[0;96m"
#define HWHT "\e[0;97m"

//Bold high intensity text
#define BHBLK "\e[1;90m"
#define BHRED "\e[1;91m"
#define BHGRN "\e[1;92m"
#define BHYEL "\e[1;93m"
#define BHBLU "\e[1;94m"
#define BHMAG "\e[1;95m"
#define BHCYN "\e[1;96m"
#define BHWHT "\e[1;97m"

//Reset
#define CLRS "\e[0m"


void debugFlagInit();
void fPrintf(int8_t *fmt, ...);


typedef struct
{
	int8_t   *s;
	int32_t  *p;
	int32_t  init_value;
}__st_set_mob_;


typedef struct
{
	uint32_t obcRX;
	uint32_t obcTX;
	uint32_t txRCV;
	uint32_t txTRN;
	uint32_t rxRCV;
	uint32_t rxTRN;
	uint32_t dbg;
	uint32_t vkey;
	uint32_t showKey;
	uint32_t adc;
	uint32_t status;

}__msg_out_bit;


#define tx_printf(...)						\
{												\
	if(mod.txTRN){							\
		fPrintf(__VA_ARGS__);					\
	}											\
}


#define rx_printf(...)						\
{												\
	if(mod.rxTRN){							\
		fPrintf(__VA_ARGS__);					\
	}											\
}

#define dbg_printf(...)						\
{												\
	if(mod.dbg){							\
		fPrintf(__VA_ARGS__);					\
	}											\
}

#define adc_printf(...)						\
{												\
	if(mod.adc){							\
		fPrintf(__VA_ARGS__);					\
	}											\
}


extern __msg_out_bit mod;
extern __st_set_mob_ set_mod[];


#endif /* INC_DEBUGPRINT_H_ */
