/*
 * ccsd.h
 *
 *  Created on: 2021. 12. 8.
 *      Author: admin
 */

#ifndef CCSD_H_
#define CCSD_H_

const uint16_t VERSION_NUMBER = 0;
const uint16_t TYPE = 0;
const uint16_t SEQUENCE_FLAGS = 3;
const uint16_t PACKET_SEQUENCE = 0;
const uint8_t TIME_ID = 0;
const uint8_t PACKET_TYPE = 0;
const uint16_t PRIMARY_HEADER_LENGTH = 6;
const uint8_t CHECKSUM_PRESENT = 0;

uint16_t SECONDARY_HEADER_FLAG = 0;
uint16_t SECONDARY_HEADER_LENGTH = 0;


#endif /* CCSD_H_ */
