/*
 * datatype.h
 *
 *  Created on: Dec 21, 2021
 *      Author: admin
 */

#ifndef DATATYPE_H_
#define DATATYPE_H_


#include "stdint.h"

#define     YES    1
#define     NO     0
#define     OFF    NO
#define     ON     YES
#define     FAIL   NO
#define     COM_FAIL	2
#define     OK     YES
#define     ENABLE YES
#define     DISABLE NO

#define flip16(x) htons(x)
#define flip32(x) htonl(x)
#define flip64(x) ( (((int64_t)htonl(x))<<32) | (htonl(x >> 32)))
#define flip64u(x) ( (((int64_t)htonl(x))<<32) | (htonl(x >> 32)))


typedef struct{
    char msgID;
    char set;
    int  szMsg;     		//message size( Byte)
    void (*Cmdfunc)(char cmd, char *buf, int len);
    void  (*Sndfunc)(char cmd, int len);
    int  timer;
    int  setTimer;
    int  event;

}OBC_Command_;


#endif /* DATATYPE_H_ */
