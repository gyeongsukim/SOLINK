/*
 * timer.h
 *
 *  Created on: Dec 13, 2021
 *      Author: admin
 */

#ifndef TIMER_H_
#define TIMER_H_

#include "stdint.h"


typedef struct
{
    volatile uint8_t  Flag;     /* Timeout event flag */
    uint16_t Timer;             /* Timeout duration in 10msec */
    uint16_t Start;			    /* timer set*/
}TimeOut_Event_t;

#endif /* TIMER_H_ */
