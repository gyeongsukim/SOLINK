/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2021 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "uartutils.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
#define _DBG_RECV_DATA_					0
#define _ENABLE_TX_MODEM_RECEIVE_ 		1
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CTRL_HALF1_Pin GPIO_PIN_0
#define CTRL_HALF1_GPIO_Port GPIOC
#define CTRL_HALF2_Pin GPIO_PIN_1
#define CTRL_HALF2_GPIO_Port GPIOC
#define SW_5V_HPA_Pin GPIO_PIN_3
#define SW_5V_HPA_GPIO_Port GPIOC
#define ax_Pin GPIO_PIN_0
#define ax_GPIO_Port GPIOB
#define ax_EXTI_IRQn EXTI0_IRQn
#define GPIO_3_Pin GPIO_PIN_2
#define GPIO_3_GPIO_Port GPIOB
#define RST_RFTX_N_Pin GPIO_PIN_4
#define RST_RFTX_N_GPIO_Port GPIOB
#define RST_RFRX_N_Pin GPIO_PIN_5
#define RST_RFRX_N_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define MAX_BUFF_LEN		256
#define	MAX_QUEUE_IDX		40
#define CALLSIGN_MSG_SIZE 	7

#define LOG_LEVEL_LOW		0				//all
#define LOG_LEVEL_MID		1				//need
#define LOG_LEVEL_HIGH		2				//none. only msg

#define LOG_LEVEL			LOG_LEVEL_HIGH	//LOG_LEVEL_MID	//LOG_LEVEL_HIGH

#define UART_TIMEOUT		1000

#define RETVAL_SUCCESS		1
#define RETVAL_FAIL			0

/* Configuration **************************************************************/
#define DMA_BUF_SIZE        256      /* DMA circular buffer size in bytes */
#define DMA_TIMEOUT_MS      20      /* DMA Timeout duration in msec */
/******************************************************************************/

enum DMA_{OBC_RX = 0, RXMODEM_RX = 1};


enum IdleStateMode {
	TrxTurnOff=0,
	TrxRemains
};

enum UartTrxBitRate {
	UartSr1200=0x1,
	UartSr2400=0x2,
	UartSr4800=0x3,
	UartSr9600=0x4
};

typedef struct
{
    volatile uint8_t  flag;     /* Timeout event flag */
    uint16_t timer;             /* Timeout duration in msec */
    uint16_t prevCNDTR;         /* Holds previous value of DMA_CNDTR */
    uint8_t channel;         /* Holds previous value of DMA_CNDTR */
}DMA_Event_t;


void PRINT_LOG(uint8_t level, char *string, uint8_t len);
void PRINT_HEX_LOG(uint8_t level, char *string, uint8_t len);
uint8_t Fn_ProcessModemMsg(void);

HAL_StatusTypeDef func_RfTxModem_Reset_manual(uint8_t set);
HAL_StatusTypeDef func_RfRxModem_Reset_manual(uint8_t set);
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
