/*
 * adc.h
 *
 *  Created on: Nov 26, 2021
 *      Author: admin
 */

#ifndef ADC_H_
#define ADC_H_



typedef struct _kalman{
	unsigned int init;
	unsigned int run_cnt;

	double x; //state vector
	double A; //state transition matrix
	double B; //input matrix
	double u; //input control vector
	double Q; //process noise covariance

	double z; //observation vector
	double H; //observation matrix

	double R; //measurement noise covariance

	double P; //covariance of the state vector estimate
}kalman;

extern kalman ADCf[2];

float func_TemperatureRead(uint32_t adc);
void func_KalmanInitAll();
double func_Kalmanf( kalman* s, double i);

#endif /* ADC_H_ */
