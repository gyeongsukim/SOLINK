/*
 * icd.h
 *
 *  Created on: 2021. 12. 14.
 *      Author: admin
 */

#ifndef ICD_H_
#define ICD_H_

#include "stdint.h"


#define OP_BYPASS   0xFF
#define OP_RESERVED 0xFF
#define PREAMBLE    0xE607


typedef struct
{
	uint16_t address;
	uint32_t data;

}OBC_SubFrame_;

typedef struct
{
	uint16_t preamble;
	uint8_t opcode;
	uint8_t length;
}OBC_ICD_Header;

typedef struct
{
	OBC_ICD_Header hdr;
	uint8_t data[256];

}OBC_ICD_ ;



#endif /* ICD_H_ */
