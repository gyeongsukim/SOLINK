/*
 * uartutils.h
 *
 *  Created on: Jan 22, 2021
 *      Author: leo
 *  Modified on: Mar 24, 2021
 *      Author: atom
 */

#ifndef UARTUTILS_H_
#define UARTUTILS_H_

#include <stdint.h>
#include <string.h>
#include "stm32f7xx_hal.h"
#include "main.h"

#define MAX_UART_SIZE					(1024 * 4)
#define MAX_OBC_UART_RXBUF_SIZE			(1024 * 16)
#define MAX_TXMDM_UART_TXBUF_SIZE		(1024 * 32)

#define KISS_FEND						0xC0
#define KISS_FESC						0xDB
#define KISS_TFEND						0xDC
#define KISS_TFESC						0xDD

#define ID_OBC_UART						4
#define ID_RXMODEM_UART					3
#define ID_TXMODEM_UART					2
#define ID_RXDEBUG_UART					250

typedef struct _UartBufType{
	uint8_t			Uid;
	uint16_t 		Head;
	uint16_t		Tail;
	uint8_t			Data[MAX_UART_SIZE];
} UartBufType;

#if 1

typedef struct _UART_BUF_KISS{
	uint8_t START;
	uint8_t ctrl;
	uint32_t seqNum;
	int16_t rssi;
	int8_t data[1024 * 1];
	int16_t dataSize;
	uint8_t END;
}UART_BUF_KISS;

UART_BUF_KISS rx_kiss_buff;

void kissFrameParser(UART_BUF_KISS *frame, int8_t * data, uint16_t datalenght);

#endif

HAL_StatusTypeDef 	func_UartBuf_Putc(UartBufType *buf, uint8_t ch);
uint8_t 			func_UartBuf_Getc(UartBufType *buf);
uint8_t				func_UartBuf_IsEmpty(UartBufType *buf);
uint16_t			func_UartBuf_GetSize(UartBufType *buf);
HAL_StatusTypeDef	func_UartBuf_Clear(UartBufType *buf);

HAL_StatusTypeDef 	func_Uart_Transmit(UART_HandleTypeDef *huart, UartBufType *buf, uint8_t *data, uint16_t len);

typedef struct _ObcUartRxBufType {
	int8_t 			Uid;
	uint16_t 		Head;
	uint16_t 		Tail;
	uint8_t			Data[MAX_OBC_UART_RXBUF_SIZE];
} ObcUartRxBufType;

HAL_StatusTypeDef 	func_ObcUartRxBuf_Putc(ObcUartRxBufType *buf, uint8_t ch);
uint8_t 			func_ObcUartRxBuf_Getc(ObcUartRxBufType *buf);
uint8_t				func_ObcUartRxBuf_IsEmpty(ObcUartRxBufType *buf);
uint16_t			func_ObcUartRxBuf_GetSize(ObcUartRxBufType *buf);
HAL_StatusTypeDef	func_ObcUartRxBuf_Clear(ObcUartRxBufType *buf);

typedef struct _TxMdmUartTxBufType {
	int8_t 			Uid;
	uint16_t 		Head;
	uint16_t 		Tail;
	uint8_t			Data[MAX_TXMDM_UART_TXBUF_SIZE];
} TxMdmUartTxBufType;

HAL_StatusTypeDef 	func_TxMdmUartTxBuf_Putc(TxMdmUartTxBufType *buf, uint8_t ch);
uint8_t 			func_TxMdmUartTxBuf_Getc(TxMdmUartTxBufType *buf);
uint8_t				func_TxMdmUartTxBuf_IsEmpty(TxMdmUartTxBufType *buf);
uint16_t			func_TxMdmUartTxBuf_GetSize(TxMdmUartTxBufType *buf);
HAL_StatusTypeDef	func_TxMdmUartTxBuf_Clear(TxMdmUartTxBufType *buf);

HAL_StatusTypeDef func_TxMdmUart_Transmit(UART_HandleTypeDef *huart, TxMdmUartTxBufType *buf, uint8_t *data, uint16_t len);

#if 0
uint16_t 			Fn_RecvModemMsg(void);
HAL_StatusTypeDef  	Fn_SendObcMsg(void);
uint16_t 			Fn_RecvObcMsg(void);
HAL_StatusTypeDef	Fn_SendModemMsg(void);
uint8_t 			Fn_ProcessObcMsg(void);
#endif

#endif /* UARTUTILS_H_ */
