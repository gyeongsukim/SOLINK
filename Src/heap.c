/*
 * heap.c
 *
 *  Created on: 2019. 1. 23.
 *      Author: HOBEOM
 */

#include <stdio.h>
#include "heap.h"

	int heap[10000];
	int heapSize;

void Init_heap(){


	heap[0] = 0;
	heapSize = 0;
}

void heap_push(int element){
	heapSize++;
	heap[heapSize] = element;

	int now = heapSize;

	while(heap[now/2] > element){
		heap[now] = heap[now/2];
		now /= 2;
	}
	heap[now] = element;
}

int heap_pop(){
	int minElement, lastElement, child, now;
	minElement = heap[1];
	lastElement = heap[heapSize--];

	for(now =1; now*2 <= heapSize; now = child){
		child = now*2;

		if(child != heapSize && heap[child +1] < heap[child]){
			child++;
		}

		if(lastElement > heap[child]){
			heap[now] = heap[child];
		}else
		{
			break;
		}
	}

	heap[now] = lastElement;
	return minElement;
}
