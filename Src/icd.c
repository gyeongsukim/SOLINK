/*
 * icd.c
 *
 *  Created on: 2021. 12. 14.
 *      Author: admin
 */

#include "icd.h"
#include "datatype.h"



#define MAX_ICD_NUM (10)
OBC_ICD_ MSG_ICD={0,};

//OBC_Command_ msgOBC[MAX_ICD_NUM] = {
///*       opcde      ,set, size(byte), receive func        , Send func                , timer , event          */
//
////{ set_Modulator          , ON , 2     , setModulator        ,        NULL              , -1,-1, OFF},
//
//
//};



int32_t opcodeChecker(uint8_t opcode)
{
	int32_t rtl = 0;

	return rtl;

}

int32_t dataLenghtChecker(uint8_t length)
{
	int32_t rtl = 0;

	if(length > 200){ 	rtl = 0; }
	else            {	rtl = 1; }

	return rtl;
}


int32_t msgPackHeader(OBC_ICD_Header* hdr, uint8_t opcode, uint8_t length)
{

	int32_t rtl = 1;
	hdr->preamble = PREAMBLE;
	if(opcodeChecker(opcode) == OK){ hdr->opcode = opcode; 	}
	else                           { rtl = 0;              }

	if(dataLenghtChecker(length) == OK) { hdr->length = length; }
	else                                { rtl = 0;            }
	return rtl;

}

int32_t msgUnPack(uint8_t *rData)
{
	int32_t rtl = 1;
	OBC_ICD_ rcv;

	memset(&rcv,0, sizeof(OBC_ICD_));

	 rcv.hdr.preamble = flip16(*(uint16_t*)(rData+0));
	 rcv.hdr.opcode   =   ((uint8_t*)(rData+2));
	 rcv.hdr.length   =   ((uint8_t*)(rData+3));

	 if(dataLenghtChecker(rcv.hdr.length) == OK)
	 {
		 memcpy(rcv.data,(rData+4), rcv.hdr.length );
		 rtl = 1;
	 }
	 else {
		 fPrintf("OBC Receive Data length Not OK");
		 rtl = 0;
	 }
	return rtl;
}


