/*
 * command.c
 *
 *  Created on: Oct 19, 2021
 *      Author: admin
 */

#include "stdio.h"
#include "stdint.h"
#include "command.h"			//user header
#include "debug.h"				//user header
#include "debugprint.h"				//user header
#include "main.h"				//user header


#define MAX_CMD_NO 10

void cmdMSG(int32_t argc, int8_t **argv);
void cmdHELP (int32_t argc, int8_t **argv);
void cmdStop(int32_t argc, int8_t **argv);
void cmdReset(int32_t argc, int8_t **argv);
void cmdCLS (int32_t argc, int8_t **argv);
void clear_mod_var();


_Command Cmds[MAX_CMD_NO] = {
	{ "HELP", 	1, cmdHELP , "command list"},
	{ "MSG", 	1, cmdMSG  , "msg flag ex) prompt>msg 'flag' or >'msg' "},
	{ "/", 		1, cmdStop , "output message clear"},
	{ "RST",	1, cmdReset ,"board Reset"},
	{ "CLS",	1, cmdCLS   ,"Clear Console"},

//	{ "CLS",	1, cmdCLS   ,"Clear Console"},
	{ NULL, 	0,NULL , NULL}
};



void cmdHELP (int32_t argc, int8_t **argv)
{
	int32_t i,j,all=0;

	if( argc == 2) all = !strcmp( argv[1],"ALL");

	for(i=0,j=0;Cmds[i].CmdStr!=NULL; i++ )
	{
		if( all || Cmds[i].v )
		{
			printf("%3d) %7s %s\r\n",j+1,Cmds[i].CmdStr,Cmds[i].Help);
			j++;
		}

	}
	return;

}

void clear_mod_var()
{
	int i;
	for(i=0; i<31 && set_mod[i].s; i++)
	{
		*(set_mod[i].p) = 0 ;
	}
}


void cmdStop(int32_t argc, int8_t **argv)
{
	clear_mod_var();
	return;
}

void cmdCLS (int32_t argc, int8_t **argv)
{
	fPrintf("%c[1J%c[0J",0x1B,0x1B);
	return;
}


void cmdReset(int32_t argc, int8_t **argv)
{

	if(argc == 2 && (strcmp(argv[1],"RX") == 0))
	{
		fPrintf("RESET Rx Modem \r\n");
		func_RfRxModem_Reset();

	}

	if(argc == 2 && (strcmp(argv[1],"BOARD") == 0))
	{
		fPrintf("RESET BOARD \r\n");
		 NVIC_SystemReset();
 		clear_mod_var();
	}



	return;
}

void cmdMSG(int32_t argc, int8_t **argv)
{
	int32_t i;

	fPrintf("\n");
	for(i=0; argc==2 && i<31 && set_mod[i].s; i++)
	{
		if( strcmp( argv[1], set_mod[i].s ) == 0 )
		{
			*(set_mod[i].p) = !( *(set_mod[i].p) );
			fPrintf("\r\nset>> %s:%d\n",set_mod[i].s,*(set_mod[i].p));
		}
	}
	for(i=0; i<31 && set_mod[i].s; i++)
	{
		fPrintf("%9s:%d ",set_mod[i].s, *set_mod[i].p);
		if((i+1)%6 == 0 ) fPrintf("\n");
	}
	return ;
}


void cmd_upper_str(uint8_t *buf)
{
	while(*buf){
		if((*buf >= 'a') && (*buf <= 'z')){
			*buf -= 'a'-'A';
		}
   		buf++;
   }
}

int32_t cmd_parse_args(int8_t *cmdline, int8_t **argv)
{
	int8_t  *tok;
	int32_t  argc = 0;
	int8_t  *delim = " \f\n\r\t\v";

	argv[argc] = NULL;

	for(tok = strtok(cmdline, delim); tok && argc<16; tok = strtok(NULL, delim)){
		argv[argc++] = tok;
	}

	return argc;
}


void cmd_exec_cmd(uint8_t *buf, int32_t len)
{
	int32_t argc,cmdlp;
	int8_t *argv[16];

	cmd_upper_str(buf);

	argc = cmd_parse_args(buf, argv);

//	for(i = 0; i < argc; i++){
//		printf("input>>  %d , %s \r\n", i, argv[i] );
//	}

	if(argc){
		cmdlp = 0;
		while((cmdlp < MAX_CMD_NO) && (Cmds[cmdlp].func!=NULL))
		{
			if( strcmp((char*)argv[0], (char*)Cmds[cmdlp].CmdStr ) == 0 ){
				Cmds[cmdlp].func( argc, argv );
				break;
			}
			cmdlp++;
		}
	}
	ShowPrompt();
}



