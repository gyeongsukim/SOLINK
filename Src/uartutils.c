/*
 * uartutils.c
 *
 *  Created on: Jan 22, 2021
 *      Author: atom
 */

#include "uartutils.h"
#include "debugprint.h"

// UART CIRCULAR BUFFER
UartBufType					uart1RxBuf; // debug serial port.
UartBufType					uart2RxBuf; // Tx Modem receive buffer.
UartBufType					uart3RxBuf; // Rx Modem receive buffer.
ObcUartRxBufType			uart4RxBuf; // OBC Modem receive buffer.

TxMdmUartTxBufType			uart2TxBuf;
UartBufType					uart3TxBuf;
UartBufType					uart4TxBuf;

extern UART_HandleTypeDef 	huart1;			// debug serial port.
extern UART_HandleTypeDef 	huart2;
extern UART_HandleTypeDef 	huart3;
extern UART_HandleTypeDef 	huart4;


UART_BUF_KISS rx_kiss_buff;

uint32_t htonl(uint32_t const net) {
    uint8_t data[4] = {};
    memcpy(&data, &net, sizeof(data));

    return ((uint32_t) data[0] << 0)
         | ((uint32_t) data[1] << 8)
         | ((uint32_t) data[2] << 16)
         | ((uint32_t) data[3] << 24);
}

uint32_t ntohl(uint32_t const net) {
    uint8_t data[4] = {};
    memcpy(&data, &net, sizeof(data));

    return ((uint32_t) data[3] << 0)
         | ((uint32_t) data[2] << 8)
         | ((uint32_t) data[1] << 16)
         | ((uint32_t) data[0] << 24);
}

uint16_t htons(uint16_t const net) {
    uint8_t data[2] = {};
    memcpy(&data, &net, sizeof(data));

    return ((uint32_t) data[0] << 0)
         | ((uint32_t) data[1] << 8);
}

uint16_t ntohs(uint16_t const net) {
    uint8_t data[2] = {};
    memcpy(&data, &net, sizeof(data));

    return ((uint32_t) data[1] << 0)
         | ((uint32_t) data[0] << 8);
}



void kissDataChecker(int8_t * dst, int8_t * src, uint16_t srclenght)
{
	int32_t i = 0, j = 0;
	uint8_t detect = 0;

	for(i = 0, j = 0; i < srclenght; i++, j++)
	{
		if( (src[i] & 0xff) == 0xdb ){
			detect = 1;
		}
		dst[j] = src[i];

		if( i != 0 && (src[i] & 0xff) == 0xDC && detect == 1 ){
			j-=1;
			dst[j] = 0xC0;
			detect = 0;
		}

		if( i != 0 && (src[i] & 0xff) == 0xDD && detect == 1 ){
			j-=1;
			dst[j] = 0xDB;
			detect = 0;
		}
	}
}

void kissFrameParser(UART_BUF_KISS *frame, int8_t * data, uint16_t datalenght)
{
	int32_t seqNum;
	int16_t rssi;
	int32_t datasize;
	int32_t i = 0;

	datasize =  datalenght - 9 ;		// sof 1, eof, 1, rssi 2, seqnum 4, control 1 => 9;

	frame->START = data[0];
	frame->ctrl = data[1];
	frame->seqNum = (*(uint32_t*)(data+2));
	frame->rssi = (*(uint16_t*)(data+6));

//	frame->seqNum = ntohl(*(uint32_t*)(data+2));
//	frame->rssi = ntohs(*(uint16_t*)(data+6));

	for(i = 0; i < datasize; i++)
	{
		frame->data[i] = data[(8+i)];
	}
	frame->dataSize = datasize;
	frame->END = data[8+i];

	printf("sof %x, dSize %d, eof %x, \r\n", frame->START, frame->dataSize, frame->END);


}

//-----------------kay line end----------------------------

uint8_t	func_UartBuf_IsEmpty(UartBufType *buf)
{
	if(buf->Head == buf->Tail)
		return 1;
	else
		return 0;
}

HAL_StatusTypeDef func_UartBuf_Putc(UartBufType *buf, uint8_t ch)
{
	buf->Data[buf->Head] = ch;

	if(buf->Head == (MAX_UART_SIZE-1))
		buf->Head = 0;
	else
		buf->Head++;

	return HAL_OK;
}

uint8_t func_UartBuf_Getc(UartBufType *buf)
{
	uint8_t tmp;

	tmp = buf->Data[buf->Tail];

	if(buf->Tail == (MAX_UART_SIZE-1))
		buf->Tail = 0;
	else
		buf->Tail++;

	return tmp;
}

uint16_t func_UartBuf_GetSize(UartBufType *buf)
{
	uint16_t nBytes;

	if(buf->Head == buf->Tail)
		nBytes = 0;
	else if(buf->Head > buf->Tail)
		nBytes = buf->Head - buf->Tail;
	else
		nBytes = MAX_UART_SIZE - (buf->Tail - buf->Head);

	return nBytes;
}


//*** Buffer Clear*//

HAL_StatusTypeDef func_UartBuf_Clear(UartBufType *buf)
{
	buf->Head = 0;
	buf->Tail = 0;

	return HAL_OK;
}
/*
 *  *buf = uart4TxBuf
 *
 */
HAL_StatusTypeDef func_Uart_Transmit(UART_HandleTypeDef *huart, UartBufType *buf, uint8_t *data, uint16_t len)
{
	uint16_t size = len;
	uint32_t reg = READ_REG(huart->Instance->CR1);
	uint16_t TxbufferSize = sizeof(buf->Data);

	__HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
	__HAL_UART_DISABLE_IT(huart, UART_IT_TC);

	if((buf->Head + len) >= (MAX_UART_SIZE-1))
	{
		size = (MAX_UART_SIZE-1) - buf->Head;				//MAX_UART_SIZE = 4K
		memcpy(&buf->Data[buf->Head], data, size);
		memcpy(&buf->Data[0], &data[size], len - size);
		buf->Head = len - size;
	}
	else
	{
		memcpy(&buf->Data[buf->Head], data, size);
		buf->Head += size;
	}

	WRITE_REG(huart->Instance->CR1, reg);

	HAL_UART_Transmit_IT(huart, &buf->Data[buf->Tail], size);

	printf("func_Uart_Transmit: %d,%d\r\n", size, len);
	return HAL_OK;
}

//**********************************************************************************************//
uint8_t	func_ObcUartRxBuf_IsEmpty(ObcUartRxBufType *buf)
{
	if(buf->Head == buf->Tail)
		return 1;
	else
		return 0;
}

HAL_StatusTypeDef func_ObcUartRxBuf_Putc(ObcUartRxBufType *buf, uint8_t ch)
{
	buf->Data[buf->Head] = ch;

	if(buf->Head == (MAX_OBC_UART_RXBUF_SIZE-1))
		buf->Head = 0;
	else
		buf->Head++;

	return HAL_OK;
}

uint8_t func_ObcUartRxBuf_Getc(ObcUartRxBufType *buf)
{
	uint8_t tmp;

	tmp = buf->Data[buf->Tail];

	if(buf->Tail == (MAX_OBC_UART_RXBUF_SIZE-1))
		buf->Tail = 0;
	else
		buf->Tail++;

	return tmp;
}

uint16_t func_ObcUartRxBuf_GetSize(ObcUartRxBufType *buf)
{
	uint16_t nBytes;

	if(buf->Head == buf->Tail)
		nBytes = 0;
	else if(buf->Head > buf->Tail)
		nBytes = buf->Head - buf->Tail;
	else
		nBytes = MAX_OBC_UART_RXBUF_SIZE - (buf->Tail - buf->Head);

	return nBytes;
}

HAL_StatusTypeDef func_ObcUartRxBuf_Clear(ObcUartRxBufType *buf)
{
	buf->Head = 0;
	buf->Tail = 0;

	return HAL_OK;
}

//**********************************************************************************************//
uint8_t	func_TxMdmUartTxBuf_IsEmpty(TxMdmUartTxBufType *buf)
{
	if(buf->Head == buf->Tail)
		return 1;
	else
		return 0;
}

HAL_StatusTypeDef func_TxMdmUartTxBuf_Putc(TxMdmUartTxBufType *buf, uint8_t ch)
{
	buf->Data[buf->Head] = ch;

	if(buf->Head == (MAX_TXMDM_UART_TXBUF_SIZE-1))
		buf->Head = 0;
	else
		buf->Head++;

	return HAL_OK;
}

uint8_t func_TxMdmUartTxBuf_Getc(TxMdmUartTxBufType *buf)
{
	uint8_t tmp;

	tmp = buf->Data[buf->Tail];

	if(buf->Tail == (MAX_TXMDM_UART_TXBUF_SIZE-1))
		buf->Tail = 0;
	else
		buf->Tail++;

	return tmp;
}

uint16_t func_TxMdmUartTxBuf_GetSize(TxMdmUartTxBufType *buf)
{
	uint16_t nBytes;

	if(buf->Head == buf->Tail)
		nBytes = 0;
	else if(buf->Head > buf->Tail)
		nBytes = buf->Head - buf->Tail;
	else
		nBytes = MAX_TXMDM_UART_TXBUF_SIZE - (buf->Tail - buf->Head);

	return nBytes;
}

HAL_StatusTypeDef func_TxMdmUartTxBuf_Clear(TxMdmUartTxBufType *buf)
{
	buf->Head = 0;
	buf->Tail = 0;

	return HAL_OK;
}

HAL_StatusTypeDef func_TxMdmUart_Transmit(UART_HandleTypeDef *huart, TxMdmUartTxBufType *buf, uint8_t *data, uint16_t len)
{
	uint16_t size = len;
	uint32_t reg = READ_REG(huart->Instance->CR1);

	__HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
	__HAL_UART_DISABLE_IT(huart, UART_IT_TC);

	if((buf->Head + len) >= (MAX_TXMDM_UART_TXBUF_SIZE-1) )
	{
		size = (MAX_TXMDM_UART_TXBUF_SIZE-1) - buf->Head;
		memcpy(&buf->Data[buf->Head], data, size);
		memcpy(&buf->Data[0], &data[size], len - size);
		buf->Head = len - size;
	}
	else
	{
		memcpy(&buf->Data[buf->Head], data, size);
		buf->Head += size;
	}

	WRITE_REG(huart->Instance->CR1, reg);
	HAL_UART_Transmit_IT(huart, &buf->Data[buf->Tail], size);

	tx_printf("func_TxMdmUart_Transmit: %d,%d(h:%d)\r\n", size, len,buf->Head);
	tx_printf("[1]STM->TxModem TxBuf<%d> Status- (H:%d/T:%d) \r\n", MAX_TXMDM_UART_TXBUF_SIZE, buf->Head, buf->Tail);
	return HAL_OK;
}

#if 0
uint16_t Fn_SendUartMsg(uartTxMsgType *uartTxMsg) {
//	HAL_
//	uartTxMsg->msg[0],
//	HAL_UART_Transmit(&huart3, (uint8_t *)string, msg_len, UART_TIMEOUT);

	return 0;
}
#endif
#if 0
uint16_t Fn_RecvModemMsg(void) {
	uint16_t retLen=0, i;
	//received uart3 rx interrupt :: flagUart3RxDone
	if(flagUart3RxDone) {
#if 0	//debug
#if 0
		HAL_UART_Transmit(&huart1, (uint8_t *)&uart3RxMsg[uart3RxArrIdxRear].msg[0], uart3RxMsg[uart3RxArrIdxRear].msgLen, UART_TIMEOUT);
#else
		for(i=0; i<uart3RxMsg[uart3RxArrIdxRear].msgLen; i++) {
			HAL_UART_Transmit(&huart1, (uint8_t *)&uart3RxMsg[uart3RxArrIdxRear].msg[i], 1, UART_TIMEOUT);
		}
#endif
#endif
	  	uart4TxMsg[uart4TxArrIdxFront].msgLen=uart3RxMsg[uart3RxArrIdxRear].msgLen;
	  	retLen=uart4TxMsg[uart4TxArrIdxFront].msgLen;
	  	memcpy(&uart4TxMsg[uart4TxArrIdxFront].msg[0], &uart3RxMsg[uart3RxArrIdxRear].msg[0], uart3RxMsg[uart3RxArrIdxRear].msgLen);
	  	uart4TxArrIdxFront=(uart4TxArrIdxFront+1)%UART_USE_MSG_BUFF_IDX;

		uart3RxMsg[uart3RxArrIdxRear].msgLen = 0;
		uart3RxArrIdxRear=(uart3RxArrIdxRear+1)%UART_USE_MSG_BUFF_IDX;
		flagUart3RxDone = 0;
	}
	return retLen;
}

uint16_t Fn_DecKissMsg(uint8_t *msg, uint16_t len) {
	uint8_t		decMsg[UART_RX_MSG_BUFF_IDX]={0,};
	uint16_t	msgIdx=0, i=0;

	for(i=0; i<len; i++) {
		if(msg[i]==KISS_FESC && msg[i+1]==KISS_TFEND) {
			decMsg[msgIdx++]=KISS_FEND;
			i++;
		}
		else if(msg[i]==KISS_FESC && msg[i+1]==KISS_TFESC) {
			decMsg[msgIdx++]=KISS_FESC;
			i++;
		}
		else {
			decMsg[msgIdx++]=msg[i];
		}
	}
	memset(msg, 0, MAX_UART_TX_MSG_LEN);
	memcpy(msg, &decMsg[0], msgIdx);
	return msgIdx;
}

uint8_t Fn_ProcessModemMsg(void) {
	if(uart4TxArrIdxFront!=uart4TxArrIdxRear) {
		uart4TxMsg[uart4TxArrIdxRear].msgLen=\
				Fn_DecKissMsg(&uart4TxMsg[uart4TxArrIdxRear].msg[0], uart4TxMsg[uart4TxArrIdxRear].msgLen);

		return RETVAL_SUCCESS;
	}
	else {
		return RETVAL_FAIL;
	}
}

uint16_t Fn_EncKissMsg(uint8_t *msg, uint16_t len) {
	uint8_t		encMsg[UART_RX_MSG_BUFF_IDX]={0,};
	uint16_t	msgIdx=0, i=0;

	encMsg[msgIdx++]=0xc0;
	for(i=0; i<len; i++) {
		if(msg[i]==KISS_FEND) {
			encMsg[msgIdx++]=KISS_FESC;
			encMsg[msgIdx++]=KISS_TFEND;
		}
		else if(msg[i]==KISS_FESC) {
			encMsg[msgIdx++]=KISS_FESC;
			encMsg[msgIdx++]=KISS_TFESC;
		}
		else {
			encMsg[msgIdx++]=msg[i];
		}
	}
	encMsg[msgIdx++]=0xc0;

	memset(msg, 0, MAX_UART_TX_MSG_LEN);
	memcpy(msg, &encMsg[0], msgIdx);

	return msgIdx;
}

HAL_StatusTypeDef Fn_SendObcMsg(void) {
	HAL_StatusTypeDef	retVal=HAL_ERROR;

	//filled tx buffer
	if(uart4TxArrIdxFront!=uart4TxArrIdxRear) {
		uart4TxMsg[uart4TxArrIdxRear].msgLen=\
				Fn_EncKissMsg(&uart4TxMsg[uart4TxArrIdxRear].msg[0], uart4TxMsg[uart4TxArrIdxRear].msgLen);
		retVal=HAL_UART_Transmit(&huart4, (uint8_t *)&uart4TxMsg[uart4TxArrIdxRear].msg[0], \
				uart4TxMsg[uart4TxArrIdxRear].msgLen, UART_TIMEOUT);
#if 0	//for debug
		HAL_UART_Transmit(&huart1, (uint8_t *)"\r\n\t\t\t\t", 6, UART_TIMEOUT);
		retVal=HAL_UART_Transmit(&huart1, (uint8_t *)&uart4TxMsg[uart4TxArrIdxRear].msg[0], \
				uart4TxMsg[uart4TxArrIdxRear].msgLen, UART_TIMEOUT);
		HAL_UART_Transmit(&huart1, (uint8_t *)"\r\n", 6, UART_TIMEOUT);
#endif

		uart4TxArrIdxRear=(uart4TxArrIdxRear+1)%UART_TX_MSG_BUFF_IDX;
//		while((HAL_UART_GetState(&huart4)&UART_FLAG_TXE)!=UART_FLAG_TXE) {};
	}
	return retVal;
}

uint16_t Fn_RecvObcMsg(void) {
	uint16_t retLen=0, i=0;
	//received uart4 rx interrupt :: flagUart4RxDone
	if(flagUart4RxDone) {
#if 1
#if 0
		HAL_UART_Transmit(&huart1, (uint8_t *)&uart4RxMsg[uart4RxArrIdxRear].msg[0], uart4RxMsg[uart4RxArrIdxRear].msgLen, UART_TIMEOUT);
#else
		for(i=0; i<uart4RxMsg[uart4RxArrIdxRear].msgLen; i++) {
			HAL_UART_Transmit(&huart1, (uint8_t *)&uart4RxMsg[uart4RxArrIdxRear].msg[i], 1, UART_TIMEOUT);
		}
#endif
#endif
		uart2TxMsg[uart2TxArrIdxFront].msgLen=uart4RxMsg[uart4RxArrIdxRear].msgLen;
	  	retLen=uart2TxMsg[uart2TxArrIdxFront].msgLen;
	  	memcpy(&uart2TxMsg[uart2TxArrIdxFront].msg[0], &uart4RxMsg[uart4RxArrIdxRear].msg[0], uart4RxMsg[uart4RxArrIdxRear].msgLen);
	  	uart2TxArrIdxFront=(uart2TxArrIdxFront+1)%UART_USE_MSG_BUFF_IDX;

		uart4RxMsg[uart4RxArrIdxRear].msgLen = 0;
		uart4RxArrIdxRear=(uart4RxArrIdxRear+1)%UART_USE_MSG_BUFF_IDX;
		flagUart4RxDone = 0;
	}
	return retLen;
}

uint8_t Fn_ProcessObcMsg(void) {
	if(uart2TxArrIdxFront!=uart2TxArrIdxRear) {
		uart2TxMsg[uart2TxArrIdxRear].msgLen=\
				Fn_DecKissMsg(&uart2TxMsg[uart2TxArrIdxRear].msg[0], uart2TxMsg[uart2TxArrIdxRear].msgLen);
		return RETVAL_SUCCESS;
	}
	else {
		return RETVAL_FAIL;
	}
}

HAL_StatusTypeDef Fn_SendModemMsg(void) {
	HAL_StatusTypeDef	retVal=HAL_ERROR;
	int i;

	//filled tx buffer
	if(uart2TxArrIdxFront!=uart2TxArrIdxRear) {
		uart2TxMsg[uart2TxArrIdxRear].msgLen=\
				Fn_EncKissMsg(&uart2TxMsg[uart2TxArrIdxRear].msg[0], uart2TxMsg[uart2TxArrIdxRear].msgLen);
#if 1	//log
		for(i=0; i<uart2TxMsg[uart2TxArrIdxRear].msgLen; i++) {
			HAL_UART_Transmit(&huart1, (uint8_t *)&uart2TxMsg[uart2TxArrIdxRear].msg[i], 1, UART_TIMEOUT);
		}
#endif
#if 1
		retVal=HAL_UART_Transmit(&huart2, (uint8_t *)&uart2TxMsg[uart2TxArrIdxRear].msg[0], \
				(uart2TxMsg[uart2TxArrIdxRear].msgLen), UART_TIMEOUT);
#else
		char	 tempVal='0';
		uint16_t uartRxLen=0;
		uint8_t  txDataBuffUart1[MAX_QUEUE_IDX][MAX_BUFF_LEN];
//		HAL_CAN_StateTypeDef canState;
//		canState=HAL_CAN_GetState(&hcan2);
		txDataBuffUart1[0][0]=tempVal++;
		txDataBuffUart1[0][1]='1';
		txDataBuffUart1[0][2]='2';
		txDataBuffUart1[0][3]='3';
		txDataBuffUart1[0][4]='4';
		txDataBuffUart1[0][5]='5';
		txDataBuffUart1[0][6]=':';
		txDataBuffUart1[0][7]='0';	//+canState;
		txDataBuffUart1[0][8]='\r';
		txDataBuffUart1[0][9]='\n';
		uartRxLen=10;
		HAL_UART_Transmit(&huart2, (uint8_t *)txDataBuffUart1[0], uartRxLen, UART_TIMEOUT);

		if(tempVal>'9') tempVal='0';
#endif
		uart2TxArrIdxRear=(uart2TxArrIdxRear+1)%UART_USE_MSG_BUFF_IDX;
//		while((HAL_UART_GetState(&huart2)&UART_FLAG_TXE)!=UART_FLAG_TXE) {};
	}
	return retVal;
}
#endif
