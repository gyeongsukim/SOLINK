/*
 * adc.c
 *
 *  Created on: Nov 26, 2021
 *      Author: admin
 */

#include "stdio.h"
#include "stdint.h"
#include "math.h"
#include "adc.h"


#define MAX_ADC 4096


kalman ADCf[2];

void func_kalman_init( kalman* s)
{
	s->x = 0;
	s->A = 1;
	//s.Q = 1;
	s->Q = 0.01;
	s->H = 1;
//	s.R = 2;
//	s.R = 1;
	s->R = 2;

	s->B = 0;
	s->u = 0;
	s->x = 0;
	s->P = 0;
	s->run_cnt = 0;
}

void func_kalman_change( kalman* s, double q, double r)
{
	s->Q = q;
	s->R = r;
}

/*
	input : kalman* s = kalman structure , double i = 값.
*/
double func_Kalmanf( kalman* s, double i)
{
	double K;
	s->z = i;
	//first run
	if(!s->run_cnt){
		s->x = (1/s->H) * s->z;
		s->P = (1/s->H)* s->R * (1/s->H);
	}
	else{
		s->x = (s->A * s->x) + (s->B * s->u);
		s->P = (s->A * s->P * s->A) + s->Q;
		K = s->P * s->H * 1/(s->H * s->P * s->H + s->R);
		s->x = s->x + K * (s->z-s->H*s->x);
		s->P = s->P - K * s->H * s->P;
	}
	s->run_cnt=1;
	return s->x;
}

void func_KalmanInitAll()
{
	func_kalman_init( &ADCf[0] );
	func_kalman_init( &ADCf[1] );
	func_kalman_change( &ADCf[0],0.015, 2);
	func_kalman_change( &ADCf[1],0.015, 2);
}


float func_TemperatureRead(uint32_t adc)
{
	float rtl = -255.0;

	double temp =0;
	double C3a=0.9277735446e-7;			//https://www.thinksrs.com/downloads/programs/therm%20calc/ntccalibrator/ntccalculator.html
	double C2a=2.324653425e-4;
	double C1a=1.141233760e-3;

	double Vi = 3.3;			// base voltage.
	double Vo = 0;              // adc --> v
	double Ra = 0;              // 기준 대비 Vo  %값
	double r = 0;               // Calc R.

	Vo = adc/(MAX_ADC/Vi);
	Ra = 1.0 - (Vo/Vi);
	r = (10000/(Vo/Vi))*Ra;			// R2 = 10Kohm(10000) = ADC 2096

	temp= 1 /( C1a+ C2a*log(r)+C3a*( pow(log(r),3.0)));			// Steinhart-Hart  formula

	rtl = (temp-273.15);			// K -> C

	return rtl;
}
