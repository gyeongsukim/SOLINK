/*
 * debugprintf.c
 *
 *  Created on: Oct 19, 2021
 *      Author: admin
 */


#include "debugprint.h"
#include "debug.h"
#include "datatype.h"
#include "stdio.h"
#include "stddef.h"
#include "string.h"
#include "stdarg.h"



void init_mod_var();

__msg_out_bit mod;		//me

__st_set_mob_ set_mod[]= {				//max 31.

	 {"ORX"	,&mod.obcRX		,0}				// OBC RX
	,{"OTX"	,&mod.obcTX		,0}				// OBC TX
	,{"TRX"	,&mod.txRCV	    ,0}				//Tx Modem status Receive
	,{"TTX"	,&mod.txTRN		,1}				//Tx Modem Transmit
	,{"RRX"	,&mod.rxRCV		,0}				//Rx Modem Data/Status Receive
	,{"RTX"	,&mod.rxTRN		,0}				//Rx Modem Setting
	,{"DBG"	,&mod.dbg		,1}
	,{"SKEY",&mod.showKey   ,0}
	,{"ADC" ,&mod.adc       ,0}
	,{"STS" ,&mod.status    ,0}
	,{NULL		,NULL		,0}
};

uint8_t string[DBGU_TX_BUF_MAX];

void debugFlagInit()
{
	init_mod_var();
}



void init_mod_var()
{
	int32_t i;

	for(i=0; i<31 && set_mod[i].s; i++)
	{
		*(set_mod[i].p) = set_mod[i].init_value ;
	}
}



int testmain()
{
    return 0;
}


void fPrintf(int8_t *fmt, ...)
{
	uint32_t len;
	uint8_t *stringf = string;
	va_list ap;

	va_start(ap,fmt);
	len = vsnprintf(string, DBGU_TX_BUF_MAX, fmt,ap);
	va_end(ap);

    if(len>=DBGU_TX_BUF_MAX || len<0){
    	stringf[DBGU_TX_BUF_MAX-1] = '\0';
    }

    dbgu_tx_que_up(stringf, strlen(stringf), NO);
//  USART_LL_Transmit(USART1, stringf, len);
//	memset(string,0,sizeof(string));

}

