/*
 * debug.c
 *
 *  Created on: Oct 19, 2021
 *      Author: kaykim
 */


#include "debug.h"
#include "stdio.h"
#include "stdarg.h"
#include "string.h"
#include "stm32f7xx_hal_uart.h"
#include "datatype.h"

#define ESC 	(0x88) //dummy
#define LEFT	(0x01)
#define UP		(0x02)
#define	DOWN  	(0x04)
#define RIGHT	(0x08)
#define MENU	(0x08)
#define ENTER	(0x0A)
#define min(x,y)	((x)<(y)?(x):(y))
#define max(x,y)	((x)>(y)?(x):(y))

_buf_scc bs_dbg_tx={0,};

_UART_BUF dbgu_rx_buf;

uint8_t debug_tx_dma_buf[DBGU_TX_DMA_BUF_MAX];
uint8_t debug_rx_frame_buf[DBGU_RX_FRAME_BUF_MAX+1] = {0,};
uint8_t RxBuffer[DBGU_RX_LOW_BUF_MAX];

static int32_t iVkey;
int32_t debug_out_formatted = NO;
int8_t debug_ubx_bypass = 0;
SYSTEM_INFO systeminfo;

void debugInit(void)
{
	uint32_t rtl = 0;

	debugFlagInit();
	rtl	 = debugBufferInit(&dbgu_rx_buf);

	if(rtl == HAL_ERROR)
	{
		printf(" Debug Init Fail. \r\n");

	} else {
		printf("Debug Init Ok. \r\n");
	}

}

uint32_t debugBufferInit(_UART_BUF *buf)
{
	uint32_t rtl = 0;

	buf->buf_size = DBGU_RX_LOW_BUF_MAX;
	buf->buf_ready = NO;
	buf->head_cur = 0;
	buf->head_lat = 0;
	buf->tail_cur = 0;
	buf->last_rx_counting = 0;
	buf->buf = RxBuffer;
	if(buf->buf == NULL){
		rtl = HAL_ERROR;
	} else {
		rtl =  HAL_OK;
	}

	return rtl;
}


void dbgu_rx_frame_parser(unsigned char* buf, int len)
{
	if(len<=0) {return;}
	cmd_exec_cmd(buf, len);
}


void push_vkey(int32_t c)
{
	iVkey = c;
}

int32_t get_vkey()
{
int key[]={0,UP,DOWN,ENTER,MENU};

	int32_t c = iVkey;
	if( iVkey ) iVkey = 0;
	else return 0;
	return key[c];
}

void dbgu_rx_low_recv(int8_t c)
{
	static int32_t bEsc = 0;
	static int8_t c_prev[3]={0,};
	char temp[16]={0,};

	_UART_BUF* p = &dbgu_rx_buf;			//dbgu_rx_buf, buffer update. data push.

	if(!debug_ubx_bypass){
		if( mod.vkey){
			if( c == 0x1B ) bEsc = 1;
			else
			if( bEsc ){
				if( c == 0x5B ) return;
				if( c>=0x41 && c<=0x44){
					push_vkey(c-0x40);
				}
				bEsc = 0;
				return;
			}
		}
		if( mod.showKey ){
			printf("\r\n ppp:0x%02X,pp:0x%02X,p:0x%02X,0x%02X\n",c_prev[2],c_prev[1], c_prev[0] , c );
		}

		if(c == 0x08){						//  (\b)   backspace keyvalue
			if(p->head_lat != p->head_cur){
				if(--p->head_cur < 0){p->head_cur = p->buf_size-1;}
				p->buf[p->head_cur]= 0;

				dbgu_tx_que_up("\b \b", strlen("\b \b"), NO);
//				USART_LL_Transmit(USART1, "\b \b", strlen("\b \b"));
//				HAL_UART_Transmit(&huart1, "\b \b", strlen("\b \b"), 1);
			}
		}
		//ctrl+c, stop debug msg.
		else if(c==0x03){
			debug_out_formatted = NO;
		}
//		else if((c == '\r') || (c == '\n') ){
		else if(c == 0x0d ){
			p->head_lat = p->head_cur;
			p->buf_ready = YES;
		}
		else{
			p->buf[p->head_cur++]= c;
			dbgu_tx_que_up(&c,1, NO);					// transmit debug serial.
//			HAL_UART_Transmit(&huart1, &c, 1, 1);

		}
	}
	else{
		p->buf[p->head_cur++]= c;
//		bypass_timeout = 1;			//2021.10.19  comment for Test
	}

	if(p->head_cur>=p->buf_size){
		p->head_cur = 0;
	}
	c_prev[2]=c_prev[1];
	c_prev[1]=c_prev[0];
	c_prev[0]=c;

	p->last_rx_counting = 0;
}



void debug_rx_frame_handle(void)
{
	_UART_BUF* p = &dbgu_rx_buf;				//dbgu_rx_buf, buffer update. data pop.
	int32_t head_temp = p->head_lat;
	int32_t i;
	int8_t c;

	// appox. 10milli (10bytes gap)
	if((p->buf_ready == YES) ) {
		p->buf_ready = NO;
//		p->last_rx_counting = 0;			//2021.10.20

		if(head_temp != p->tail_cur){

			memset(debug_rx_frame_buf, 0, DBGU_RX_FRAME_BUF_MAX+1);

			for(i=0;i<DBGU_RX_FRAME_BUF_MAX;){
				c = p->buf[p->tail_cur++];
				debug_rx_frame_buf[i++] = c ;
				if(p->tail_cur>=p->buf_size){
					p->tail_cur = 0;
				}
				if(p->tail_cur==head_temp){
					break;
				}
			}
			if(!debug_ubx_bypass){
				debug_rx_frame_buf[i] = 0;
				dbgu_rx_frame_parser(debug_rx_frame_buf, i);
			}
			else{
				debug_rx_frame_buf[i]=0;
//				printf(debug_rx_frame_buf);
			}
		}
		else{
			if(!debug_ubx_bypass){
				ShowPrompt();
			}
		}
	}
}


void dbgu_tx_que_up(uint8_t* buf, int32_t size, int32_t from_isr)
{
	int i;
	for(i=0; i< min(size, M_SIZE_BUF_TROUGH); i++ )
	{
		_buf_scc_push(&bs_dbg_tx, buf[i] );
	}
}

void _buf_scc_push(_buf_scc *p, uint8_t  c )
{

	if( p->c_prev!='\r' && c=='\n' )
	{
		p->buf[p->h]='\r';
		p->h++;
		if( p->h >=M_SIZE_BUF_TROUGH ) p->h = 0;
	}
	p->buf[p->h]=c;
	p->h++;
	if( p->h >=M_SIZE_BUF_TROUGH ) p->h = 0;

	p->c_prev = c;
}

int dbgu_tx_empty(void)
{
	int rtl = 1;

//	if( LL_USART_IsActiveFlag_TC(USART1) && LL_USART_IsActiveFlag_TXE(USART1) ){rtl = YES;}
//	else{rtl = NO;}

	return rtl;
}


void dbgu_tx_que_down(UART_HandleTypeDef* huart)				// transfer call
{
	int32_t size=0;
	int8_t c;
	int8_t buf[DBGU_TX_DMA_BUF_MAX];

//	if( dbgu_tx_empty() == YES)
//	{
		while( _buf_scc_pop(&bs_dbg_tx, &c) && size < DBGU_TX_DMA_BUF_MAX)
		{
			buf[size] = c;
			size++;
		}
		if( size )
		{
//			USART_LL_Transmit(USART1, buf, size); // 실제 출력 되는 부분.
			HAL_UART_Transmit_IT(huart, buf, size);
		}
//	}
}

int _buf_scc_pop(_buf_scc *p, char* c )
{
	if( p->h == p->t ) return 0;
	*c = p->buf[ p->t ];
	p->t++;
	if( p->t >=M_SIZE_BUF_TROUGH ) p->t = 0;
	return 1;
}


void ShowPrompt()
{
	uint8_t product[16];

	sprintf(product,"\n%s>", PRODUCT );
	dbgu_tx_que_up(product, strlen(product), NO);
}

void ShowLogo()
{
	printf(" \r\n");
	printf("│ ╔═╗┌─┐┬  ┌─┐╔╦╗┌─┐┌─┐\r\n");
	printf("│ ╚═╗│ ││  ├┤  ║ │ │├─┘\r\n");
	printf("│ ╚═╝└─┘┴─┘└─┘ ╩ └─┘┴  \r\n");
	printf("ver.%s %s \r\n",__DATE__, __TIME__);
	ShowPrompt();
}

void ShowStatus()
{
	printf("%c[1J%c[0J",0x1B,0x1B);			//clear screen
	printf("%c[1;1H--------<<  SOLINK STATUS MONITOR  >>--------------\r\n",0x1B);
	printf("Running Time :  %d \r\n", systeminfo.runningTime);
	printf("OBC Status : %s ", systeminfo.obc ? "Good":"not Good" );
	printf(" || Rx Count : 1 "  );			//Todo.
	printf(" || Tx Count : 2 \r\n"  );			//Todo.
	printf("RX Status  : %s ",systeminfo.RxMODEM ? "Good":"not Good");
	printf(" || Rx Count : 1 "  );			//Todo.
	printf(" || Tx Count : 2 \r\n"  );			//Todo.
	printf("TX Status  : %s ",systeminfo.TxMODEM ? "Good":"not Good");
	printf(" || Rx Count : 1 "  );			//Todo.
	printf(" || Tx Count : 2 \r\n"  );			//Todo.
	printf("Temperature Sensor 0 : %3d 'C\r\n",systeminfo.adc[0].celsius);
	printf("Temperature Sensor 1 : %3d 'C\r\n",systeminfo.adc[1].celsius);
	printf("-----------------------------------------------------\r\n");
}


