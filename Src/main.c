/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2021 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stm32f7xx_it.h"
#include "debug.h"
#include "debugprint.h"
#include "command.h"
#include "adc.h"
#include "timer.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#define MAX_BUF_SIZE				(1024 * 4)

#define MAX_RFTX_BUF_SIZE			(1024 * 1)
#define MAX_RFTX_DATA_SIZE			200
#define MAX_RFTX_SIZE				220

#define ID_FRAME_OBC				4
#define ID_FRAME_RXMODEM			3
#define ID_FRAME_TXMODEM			2
#define ID_DEBUG_CONSOLE			250
#define ARRAY_LEN(x)            (sizeof(x) / sizeof((x)[0]))

typedef struct _QueueBufType{
	uint8_t			Fid;
	uint16_t		Length;
	uint8_t			Data[MAX_BUF_SIZE];
} QueueBufType;

typedef struct _RfTxQueueBufType{
	uint16_t		Head;
	uint16_t		Tail;
	uint8_t 		Data[MAX_RFTX_BUF_SIZE];
} RfTxQueueBufType;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define CTRL 		0x03
#define PID 		0xF0

#define ST_KISS_FEND		0
#define ST_KISS_FESC		1
#define ST_KISS_DATA		2

#define KISS_CODE_DATA              0x00
#define KISS_CODE_TDELAY            0x01
#define KISS_CODE_P                 0x02
#define KISS_CODE_SLOT_TIME         0x03
#define KISS_CODE_TX_TAIL           0x04
#define KISS_CODE_FULL_DUPLEX       0x05
#define KISS_CODE_MODEM_CTRL        0x06
#define KISS_CODE_RETURN            0xFF
#define TX_MODEM_TRANSMIT_START 	0xEE
#define TX_MODEM_TRANSMIT_END       KISS_CODE_RETURN

#define _ENABLE_TXMODEM_SEQUENCE_NUM_				0
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */


/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart4;
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_uart4_rx;
DMA_HandleTypeDef hdma_usart3_rx;

/* USER CODE BEGIN PV */

//callsign
char 				msg_sat_callsign[10] = "ISISST";
char 				msg_gs_callsign[10] = "D90HP";
uint8_t				msg_sat_callsign_ascii[10] = {0,};
uint8_t				msg_gs_callsign_ascii[10] = {0,};

//beacon
uint8_t 			flag_1sec = 0, flag_beacon = 0;
uint32_t			beacon_cal_time_sec = 0, beacon_send_time_sec = 0;
uint8_t 			beacon_msg[MAX_BUFF_LEN] = {0,};
uint8_t				beacon_msg_len = 0;
//DMA


DMA_Event_t dma_uart_obc_rx = {0,0,0,0};				//channel = 0 : OBC receive, //channel  = 1 :  RX MODEM receive
DMA_Event_t dma_uart_rxm_rx = {0,0,0,1};				//channel = 0 : OBC receive, //channel  = 1 :  RX MODEM receive
uint8_t dma_rx_buf[2][DMA_BUF_SIZE];       /* Circular buffer for DMA */


extern UartBufType					uart1RxBuf; // debug serial port.

extern UartBufType					uart2RxBuf;
extern UartBufType					uart3RxBuf;
extern ObcUartRxBufType				uart4RxBuf;
extern TxMdmUartTxBufType			uart2TxBuf;
extern UartBufType					uart3TxBuf;
extern UartBufType					uart4TxBuf;

uint8_t				recvRxModemKissState = 0;
uint8_t				recvObcKissState = 0;
uint8_t				recvTxModemKissState = 0;
uint8_t				recvDebugConsoleState = 0;

RfTxQueueBufType lToTxModemBuf;

uint8_t rfTxModemUartReady = 1;
uint32_t rfTxModemTrasmitOK = 0;			//kaykim. 2021.11.08.



/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_UART4_Init(void);
static void MX_TIM3_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */
void 		callsign_from_ascii(void);
void 		PRINT_LOG(uint8_t level, char *string, uint8_t len);
uint16_t 	check_len(uint32_t *arrAddr);

#ifdef __cplusplus
extern "C" int _write(int32_t file, uint8_t *ptr, int32_t len) {
#else
int _write(int32_t file, uint8_t *ptr, int32_t len) {
#endif
    if( HAL_UART_Transmit(&huart1, ptr, len, len) == HAL_OK ) return len;
    else return 0;
}

extern TimeOut_Event_t RxModemTimer;

uint8_t func_QueueBuf_IsEmpty(QueueBufType *buf);
HAL_StatusTypeDef func_QueueBuf_Putc(QueueBufType *buf, uint8_t ch);
uint16_t func_QueueBuf_GetSize(QueueBufType *buf);
HAL_StatusTypeDef func_QueueBuf_Clear(QueueBufType *buf);

uint16_t func_MakeKissFrame(uint8_t *frame, uint8_t *data, uint16_t len, uint8_t convert);
uint16_t func_EncToKissData(uint8_t *data, uint16_t len, uint8_t *enc_data);
HAL_StatusTypeDef func_FrameDecoding(QueueBufType *buf);
int func_BypassKissData(UartBufType *ubuf, uint8_t *state, QueueBufType *qbuf);
int func_DecKissData(UartBufType *ubuf, uint8_t *state, QueueBufType *qbuf);

HAL_StatusTypeDef func_SendToRfTxUart(uint8_t *data, uint16_t len);
HAL_StatusTypeDef func_RfTxQueueBuf_PutArray(RfTxQueueBufType *buf, uint8_t *data, uint16_t len);
int32_t DMAStreamBufferRead(UART_HandleTypeDef *huart, DMA_Event_t *uart_rx, uint8_t *state, QueueBufType *qbuf);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
HAL_StatusTypeDef func_RfTxModem_Reset()
{
	HAL_GPIO_WritePin(GPIOB, RST_RFTX_N_Pin, GPIO_PIN_RESET);
	HAL_Delay(10);
	HAL_GPIO_WritePin(GPIOB, RST_RFTX_N_Pin, GPIO_PIN_SET);

	return HAL_OK;
}

HAL_StatusTypeDef func_RfRxModem_Reset()
{
	HAL_GPIO_WritePin(GPIOB, RST_RFRX_N_Pin, GPIO_PIN_RESET);
	HAL_Delay(10);
	HAL_GPIO_WritePin(GPIOB, RST_RFRX_N_Pin, GPIO_PIN_SET);

	return HAL_OK;
}


HAL_StatusTypeDef func_RfTxModem_Reset_manual(uint8_t set)
{
	uint8_t rtl = HAL_BUSY;

	if(set != 0  && set != 1)
	{
		rtl = HAL_ERROR;
	} else {
		HAL_GPIO_WritePin(GPIOB, RST_RFTX_N_Pin, set);
		rtl = HAL_OK;
	}
	return rtl;
}

HAL_StatusTypeDef func_RfRxModem_Reset_manual(uint8_t set)		// RX modem Manual Reset
{
	uint8_t rtl = HAL_BUSY;

	if(set != 0  && set != 1)
	{
		rtl = HAL_ERROR;
	} else {
		HAL_GPIO_WritePin(GPIOB, RST_RFRX_N_Pin, set);
		rtl = HAL_OK;
	}

	return rtl;
}

uint8_t func_QueueBuf_IsEmpty(QueueBufType *buf)
{
	if(buf->Length == 0)
		return 1;
	else
		return 0;
}

/** data insert.
 *
 *
 **/

HAL_StatusTypeDef func_QueueBuf_Putc(QueueBufType *buf, uint8_t ch)
{
	buf->Data[buf->Length] = ch;

	if(buf->Length == (MAX_BUF_SIZE-1))
		buf->Length = 0;
	else
		buf->Length++;

	return HAL_OK;
}

/** buffer size getdata insert.
 *
 *
 **/

uint16_t func_QueueBuf_GetSize(QueueBufType *buf)
{
	return buf->Length;
}

HAL_StatusTypeDef func_QueueBuf_Clear(QueueBufType *buf)
{
	buf->Length = 0;

	return HAL_OK;
}

uint16_t func_EncToKissData(uint8_t *data, uint16_t len, uint8_t *enc_data)
{
	uint16_t i = 0;
	uint16_t encSize = 0;

	//enc_data[encSize++] = KISS_FEND;
	//enc_data[encSize++] = 0x00;
	for(i = 0; i < len; i++)
	{
		if(data[i] == KISS_FEND)
		{
			enc_data[encSize++] = KISS_FESC;
			enc_data[encSize++] = KISS_TFEND;
		}
		else if(data[i] == KISS_FESC)
		{
			enc_data[encSize++] = KISS_FESC;
			enc_data[encSize++] = KISS_TFESC;
		}
		else
		{
			enc_data[encSize++] = data[i];
		}
	}
	//enc_data[encSize++] = KISS_FEND;

	return encSize;
}

uint16_t func_MakeKissFrame(uint8_t *frame, uint8_t *data, uint16_t len, uint8_t convert)
{
	uint16_t lFrmCnt = 0;
	uint16_t rtl = 0;


	if(convert){

		frame[lFrmCnt++] = KISS_FEND;
		frame[lFrmCnt++] = KISS_CODE_DATA;
		lFrmCnt += func_EncToKissData(data, len, &frame[lFrmCnt]);
		frame[lFrmCnt++] = KISS_FEND;
		rtl = lFrmCnt;
	} else {

		memcpy((void *)frame,(void *)data, len);
		rtl = len;
	}


	return rtl;
}

HAL_StatusTypeDef func_SendToRfTxUart(uint8_t *data, uint16_t len)				//tx modem only
{
	uint16_t lKissEncSize = 0;
	uint8_t lKissEncData[MAX_BUF_SIZE];

	uint16_t i;

	lKissEncSize = func_MakeKissFrame(lKissEncData, data, len, 0);

	tx_printf("\r\n 1tx tick counter %d , %d(0x%04x) \r\n",tick100ms,  rfTxModemTrasmitOK , rfTxModemTrasmitOK);
	func_RfRxModem_Reset_manual(RESET);

	func_TxMdmUart_Transmit(&huart2, &uart2TxBuf, lKissEncData, lKissEncSize);

	tx_printf("2tx tick counter %d \r\n",tick100ms);

	//HAL_UART_Transmit(&huart2, &lKissEncData[0], lKissEncSize, 10000);

	return HAL_OK;
}

HAL_StatusTypeDef func_FrameDecoding(QueueBufType *buf)
{
	uint8_t tmp;

	tmp = buf->Data[0];

    switch(tmp)
    {

    	case KISS_CODE_TDELAY:
        case KISS_CODE_P:
        case KISS_CODE_SLOT_TIME:
        case KISS_CODE_TX_TAIL:
        case KISS_CODE_FULL_DUPLEX:

            break;

        case KISS_CODE_RETURN:
        	break;
/**
        case TX_MODEM_TRANSMIT_END:
        	if(buf->Fid == ID_FRAME_TXMODEM)
        	{
        		rfTxModemUartReady = 1;
        		printf("TxModem Transmit end... FLAG:%d\r\n", rfTxModemUartReady);
        	}

            break;
        case TX_MODEM_TRANSMIT_START:
        	if(buf->Fid == ID_FRAME_TXMODEM)
        	{
        		rfTxModemUartReady = 0;
        		printf("TxModem Transmit start... FLAG:%d\r\n", rfTxModemUartReady);
        	}
        	break;
**/
        case KISS_CODE_MODEM_CTRL:
        	if(buf->Fid == ID_FRAME_OBC)
        	{
        		if(buf->Data[1] == 0x01) { func_RfTxModem_Reset(); printf("Tx Modem Reset \r\n"); }
        		else if(buf->Data[1] == 0x02) { func_RfRxModem_Reset(); printf("Rx Modem Reset \r\n");}
        		else if(buf->Data[1] == 0x03) { func_RfTxModem_Reset(); func_RfRxModem_Reset(); printf("Rx&Tx Modem Reset \r\n");}
        	}
            break;

        case KISS_CODE_DATA:
        	if(buf->Fid == ID_FRAME_OBC)
        	{
//        		func_SendToRfTxUart(&buf->Data[1], buf->Length-1);
        		//lTmpBufIdx = func_EncKissData(&buf->Data[1], buf->Length-1, &lTmpBuf[0]);
        		//func_RfTxQueueBuf_PutArray(&lToTxModemBuf, &lTmpBuf[0], lTmpBufIdx);
        		//func_RfTxQueueBuf_PutArray(&lToTxModemBuf, &buf->Data[1], buf->Length-1);
        	}
        	break;
        default:

        	break;
    }

    return HAL_OK;
}

int func_BypassRawData(UartBufType *ubuf, QueueBufType *qbuf)
{
	if(func_UartBuf_IsEmpty(ubuf))
		return -1;
	else
	{
		while(!func_UartBuf_IsEmpty(ubuf))
		{
			func_QueueBuf_Putc(qbuf, func_UartBuf_Getc(ubuf));
		}
		return 1;
	}
}

/*
 * Bypass KISS data, uart4
 *
 * qbuf = lRxModemRcvFrameBuf
 *
 */

int func_BypassKissData(UartBufType *ubuf, uint8_t *state, QueueBufType *qbuf)
{
	uint8_t tmp;
	static uint8_t dbg_temp[1024]={0,};
	static uint8_t dbg_sendtemp[1024]={0,};
	static uint16_t idx = 0;
	uint32_t frmNum = 0;

	while(!func_UartBuf_IsEmpty(ubuf))
	{
		//printf("func_DecKissDaa \r\n");
//		printf("rxBuf [%d] Head:%d/Tail:%d\r\n", func_UartBuf_GetSize(ubuf), ubuf->Head, ubuf->Tail);
		tmp = func_UartBuf_Getc(ubuf);

//		printf(" 0x%02x, \r\n", tmp);
		switch(*state)
		{
			case ST_KISS_FEND:
				if(tmp == KISS_FEND)
				{
					*state = ST_KISS_DATA;
					dbg_temp[idx++] = tmp;		//
					func_QueueBuf_Putc(qbuf, tmp);
					//if(qbuf->Fid == ID_FRAME_OBC) { printf("= FRAME START =\r\n"); }
					{ printf("=> FRAME START = tick %d = \r\n",tick100ms); }
				}
				else
				{
					*state = ST_KISS_FEND;
				}
				break;

			case ST_KISS_FESC:
				if(tmp == KISS_TFEND)
				{
					*state = ST_KISS_DATA;
					func_QueueBuf_Putc(qbuf, tmp);
					dbg_temp[idx++] = tmp;		//
				}
				else if(tmp == KISS_TFESC)
				{
					*state = ST_KISS_DATA;
					func_QueueBuf_Putc(qbuf, tmp);
					dbg_temp[idx++] = tmp;		//
				}
				else
				{
					*state = ST_KISS_FEND;
					func_QueueBuf_Clear(qbuf);
				}
				break;

			case ST_KISS_DATA:
				func_QueueBuf_Putc(qbuf, tmp);
				dbg_temp[idx++] = tmp;		//
				if(tmp == KISS_FEND)
				{
					*state = ST_KISS_FEND;
					//if(qbuf->Fid == ID_FRAME_OBC) { printf("= FRAME END =\r\n"); }
					memcpy((void*)&frmNum, &dbg_temp[2], 4);

					if(qbuf->Fid == ID_FRAME_RXMODEM) { printf("= FRAME END = %0d, tick %d \r\n", ntohl(frmNum), tick100ms); }
//					kissDataChecker(dbg_sendtemp,dbg_temp,idx);
//					kissFrameParser(&rx_kiss_buff,dbg_sendtemp,idx);
//					func_SendToRfTxUart(rx_kiss_buff.data,rx_kiss_buff.dataSize);
//					memset(&rx_kiss_buff,0,sizeof(rx_kiss_buff));
//					memset(dbg_temp,0,1024);
					idx = 0;

					return 1;
				}
				else if(tmp == KISS_FESC)
				{
					*state = ST_KISS_FESC;
				}
				else
				{
					*state = ST_KISS_DATA;
				}
				break;
		}
	}

	return -1;
}

/*	ubuf = uart2RxBuf
 *  state = recvTxModemKissState
 *  qbuf = lTxModemRcvFrameBuf
 *
 *  &uart2RxBuf, &recvTxModemKissState, &lTxModemRcvFrameBuf);
 */
int func_DecKissData(UartBufType *ubuf, uint8_t *state, QueueBufType *qbuf)
{
	uint8_t tmp;

	while(!func_UartBuf_IsEmpty(ubuf))
	{
//		dbg_printf("func_DecKissDaa \r\n");
//		dbg_printf("rxBuf [%d] Head:%d/Tail:%d\r\n", func_UartBuf_GetSize(ubuf), ubuf->Head, ubuf->Tail);

		tmp = func_UartBuf_Getc(ubuf);
		switch(*state)
		{
			case ST_KISS_FEND:
				if(tmp == KISS_FEND)
				{
					*state = ST_KISS_DATA;
//					if(qbuf->Fid == ID_FRAME_OBC) { printf("TxModem= FRAME START =\r\n"); }
					if(qbuf->Fid == ID_FRAME_TXMODEM) { dbg_printf("TxModem= FRAME START =\r\n"); }
				}
				else
				{
					*state = ST_KISS_FEND;
				}
				break;

			case ST_KISS_FESC:
				if(tmp == KISS_TFEND)
				{
					*state = ST_KISS_DATA;
					func_QueueBuf_Putc(qbuf, KISS_FEND);
				}
				else if(tmp == KISS_TFESC)
				{
					*state = ST_KISS_DATA;
					func_QueueBuf_Putc(qbuf, KISS_FESC);
				}
				else
				{
					*state = ST_KISS_FEND;
					func_QueueBuf_Clear(qbuf);
				}
				break;

			case ST_KISS_DATA:
				if(tmp == KISS_FEND)
				{
					*state = ST_KISS_FEND;
//					func_FrameDecoding(qbuf);
//					if(qbuf->Fid == ID_FRAME_OBC) { printf("TxModem= FRAME END =\r\n"); }
					if(qbuf->Fid == ID_FRAME_TXMODEM) { dbg_printf("TxModem= FRAME END =\r\n"); }
					return 1;
				}
				else if(tmp == KISS_FESC)
				{
					*state = ST_KISS_FESC;
				}
				else
				{
					func_QueueBuf_Putc(qbuf, tmp);
					*state = ST_KISS_DATA;
				}
				break;
		}
	}

	return -1;
}

/*
 * ubuf = uart4RxBuf
 * state = recvObcKissState
 * qbuf = lObcRcvFrameBuf
 *
 * fuc : data Receive, from OBC.
 *
 */

int func_DecKissDataFromObc(ObcUartRxBufType *ubuf, uint8_t *state, QueueBufType *qbuf)
{
	uint8_t tmp;

	while(!func_ObcUartRxBuf_IsEmpty(ubuf))
	{
		//printf("func_DecKissDaa \r\n");
//		dbg_printf("rxBuf [%d] Head:%d/Tail:%d\r\n", func_UartBuf_GetSize(ubuf), ubuf->Head, ubuf->Tail);
		tmp = func_ObcUartRxBuf_Getc(ubuf);
//		printf(" %c (0x%02x)\r\n", tmp, tmp);

		switch(*state)
		{
			case ST_KISS_FEND:
				if(tmp == KISS_FEND)
				{
					*state = ST_KISS_DATA;
					{ printf("#> FRAME START =\r\n"); }
				}
				else
				{
					*state = ST_KISS_FEND;
				}
				break;

			case ST_KISS_FESC:
				if(tmp == KISS_TFEND)
				{
					*state = ST_KISS_DATA;
					func_QueueBuf_Putc(qbuf, KISS_FEND);
				}
				else if(tmp == KISS_TFESC)
				{
					*state = ST_KISS_DATA;
					func_QueueBuf_Putc(qbuf, KISS_FESC);
				}
				else
				{
					*state = ST_KISS_FEND;
					func_QueueBuf_Clear(qbuf);
				}
				break;

			case ST_KISS_DATA:
				if(tmp == KISS_FEND)
				{
					*state = ST_KISS_FEND;
					func_FrameDecoding(qbuf);
					{ printf("#> FRAME END =\r\n"); }
					return 1;
				}
				else if(tmp == KISS_FESC)
				{
					*state = ST_KISS_FESC;
				}
				else
				{
					func_QueueBuf_Putc(qbuf, tmp);
					*state = ST_KISS_DATA;
				}
				break;
		}
	}

	return -1;
}

//***************************************************************************************//
uint8_t func_RfTxQueueBuf_IsEmpty(RfTxQueueBufType * buf)
{
	if(buf->Head == buf->Tail)
		return 1;
	else
		return 0;
}

uint16_t func_RfTxQueueBuf_GetSize(RfTxQueueBufType *buf)
{
	uint16_t nBytes;

	if(buf->Head == buf->Tail)
		nBytes = 0;
	else if(buf->Head > buf->Tail)
		nBytes = buf->Head - buf->Tail;
	else
		nBytes = MAX_RFTX_BUF_SIZE - (buf->Tail - buf->Head);

	return nBytes;
}

HAL_StatusTypeDef func_RfTxQueueBuf_Clear(RfTxQueueBufType *buf)
{
	buf->Head = 0;
	buf->Tail = 0;

	return HAL_OK;
}

HAL_StatusTypeDef func_RfTxQueueBuf_PutArray(RfTxQueueBufType *buf, uint8_t *data, uint16_t len)
{
	uint16_t size = len;

	if((buf->Head + len) > MAX_RFTX_BUF_SIZE)
	{
		size = MAX_RFTX_BUF_SIZE - buf->Head;
		memcpy(&buf->Data[buf->Head], data, size);
		memcpy(&buf->Data[0], &data[size], len - size);
		buf->Head = len - size;
	}
	else
	{
		memcpy(&buf->Data[buf->Head], data, size);
		buf->Head += size;
	}

	return HAL_OK;
}

HAL_StatusTypeDef func_RfTxQueueBuf_GetArray(uint8_t *data, RfTxQueueBufType *buf, uint16_t len)
{
	uint16_t size = len;
	uint16_t idx = 0;

	if((buf->Tail+len) > MAX_RFTX_BUF_SIZE)
	{
		size = MAX_RFTX_BUF_SIZE - buf->Tail;
		memcpy(&data[idx], &buf->Data[buf->Tail], size); idx += size;
		memcpy(&data[idx], &buf->Data[0], len - size);
		buf->Tail = len - size;
	}
	else
	{
		memcpy(&data[idx], &buf->Data[buf->Tail], size); idx += size;
		buf->Tail += size;
	}

	return HAL_OK;
}

uint16_t rfTxModemTxCnt = 1;

HAL_StatusTypeDef func_SendToRfTxUart_Split(RfTxQueueBufType *buf)
{
	uint16_t len = 0;
	uint16_t size = 0;
	uint16_t lTmpBufIdx = 0;
	uint8_t lTmpBuf[MAX_RFTX_SIZE];

	//if(!func_RfTxQueueBuf_IsEmpty(buf) && (rfTxModemUartReady == 1))
	if(!func_RfTxQueueBuf_IsEmpty(buf))
	{
		size = func_RfTxQueueBuf_GetSize(buf);
		printf("[Tx: %04X] RfTxQueue: %d (H:%d/T:%d)\r\n", rfTxModemTxCnt, size, buf->Head, buf->Tail);

		if(size >= MAX_RFTX_DATA_SIZE)
		{
			size = MAX_RFTX_DATA_SIZE;
		}

#if 0
		//lTmpBufIdx = func_EncKissData(&buf->Data[buf->Tail], len, &lTmpBuf);

		buf->Tail += size;
		if(buf->Tail >= MAX_RFTX_BUF_SIZE)
		{
			buf->Tail -= MAX_RFTX_BUF_SIZE;
		}
#else
		lTmpBuf[lTmpBufIdx++] = KISS_FEND;
		lTmpBuf[lTmpBufIdx++] = KISS_CODE_DATA;

#if _ENABLE_TXMODEM_SEQUENCE_NUM_
		uint8_t lSeqNum[2];

		if(size >= MAX_RFTX_DATA_SIZE-2)
		{
			size = MAX_RFTX_DATA_SIZE-2;
		}

		lSeqNum[0] = (rfTxModemTxCnt >> 8) & 0xFF;
		lSeqNum[1] = (rfTxModemTxCnt & 0xFF);

		lTmpBufIdx += func_EncKissData(&lSeqNum[0], 2, &lTmpBuf[lTmpBufIdx]);

		//lTmpBuf[lTmpBufIdx++] = (rfTxModemTxCnt >> 8) & 0xFF;
		//lTmpBuf[lTmpBufIdx++] = (rfTxModemTxCnt & 0xFF);
		func_RfTxQueueBuf_GetArray(&lTmpBuf[lTmpBufIdx], buf, size); lTmpBufIdx += size;
#else
		func_RfTxQueueBuf_GetArray(&lTmpBuf[lTmpBufIdx], buf, size); lTmpBufIdx += size;
#endif

		lTmpBuf[lTmpBufIdx++] = KISS_FEND;

#endif

#if 1
		//func_Uart_Transmit(&huart2, &uart2TxBuf, &lTmpBuf[0], lTmpBufIdx); rfTxModemUartReady = 0;
		HAL_UART_Transmit(&huart2, &lTmpBuf[0], lTmpBufIdx, 1000); rfTxModemUartReady = 0;
#else
		HAL_UART_Transmit_IT(&huart2, &lTmpBuf[0], lTmpBufIdx); rfTxModemUartReady = 0;
#endif

		//HAL_Delay(1000);
		//HAL_Delay(500);
		HAL_Delay(200);
		//HAL_Delay(100);

		rfTxModemTxCnt++;
	}

	return HAL_OK;
}

void test_function(void) {
	int i;
#if 0	//uart interrupt test :: done
		if(flagUart1RxDone) {
#if 0
			HAL_UART_Transmit(&huart1, (uint8_t *)&uart1RxMsg[uart1RxArrIdxRear].msg[0], uart1RxMsg[uart1RxArrIdxRear].msgLen, UART_TIMEOUT);
#else
			for(i=0; i<uart1RxMsg[uart1RxArrIdxRear].msgLen; i++) {
				HAL_UART_Transmit(&huart1, (uint8_t *)&uart1RxMsg[uart1RxArrIdxRear].msg[i], 1, UART_TIMEOUT);
			}
#endif
			uart1RxMsg[uart1RxArrIdxRear].msgLen = 0;
			uart1RxArrIdxRear = (uart1RxArrIdxRear+1)%MAX_QUEUE_IDX;
			flagUart1RxDone = 0;
		}
		if(flagUart4RxDone) {
#if 0
			HAL_UART_Transmit(&huart1, (uint8_t *)&uart4RxMsg[uart4RxArrIdxRear].msg[0], uart4RxMsg[uart4RxArrIdxRear].msgLen, UART_TIMEOUT);
#else
			for(i=0; i<uart4RxMsg[uart4RxArrIdxRear].msgLen; i++) {
				HAL_UART_Transmit(&huart1, (uint8_t *)&uart4RxMsg[uart4RxArrIdxRear].msg[i], 1, UART_TIMEOUT);
			}
#endif
			uart4RxMsg[uart4RxArrIdxRear].msgLen = 0;
			uart4RxArrIdxRear = (uart4RxArrIdxRear+1)%MAX_QUEUE_IDX;
			flagUart4RxDone = 0;
		}
#endif
#if 0	//uart tx test : done
		char	 tempVal='0';
		uint16_t uartRxLen=0;
		uint8_t  txDataBuffUart1[MAX_QUEUE_IDX][MAX_BUFF_LEN];
//		HAL_CAN_StateTypeDef canState;
//		canState=HAL_CAN_GetState(&hcan2);
		txDataBuffUart1[0][0]=tempVal++;
		txDataBuffUart1[0][1]='1';
		txDataBuffUart1[0][2]='2';
		txDataBuffUart1[0][3]='3';
		txDataBuffUart1[0][4]='4';
		txDataBuffUart1[0][5]='5';
		txDataBuffUart1[0][6]=':';
		txDataBuffUart1[0][7]='0';	//+canState;
		txDataBuffUart1[0][8]='\r';
		txDataBuffUart1[0][9]='\n';
		uartRxLen=10;
//		HAL_UART_Transmit(&huart1, (uint8_t *)txDataBuffUart1[0], uartRxLen, UART_TIMEOUT);
		HAL_UART_Transmit(&huart2, (uint8_t *)txDataBuffUart1[0], uartRxLen, UART_TIMEOUT);
//		HAL_UART_Transmit(&huart3, (uint8_t *)txDataBuffUart1[0], uartRxLen, UART_TIMEOUT);
//		HAL_UART_Transmit(&huart4, (uint8_t *)txDataBuffUart1[0], uartRxLen, UART_TIMEOUT);

		if(tempVal>'9') tempVal='0';
#endif
#if 0	//uart rx test : test not yet
		HAL_StatusTypeDef retVal;
		int i;
		retVal=HAL_UART_Receive(&huart4, (uint8_t *)&uart4RxMsg[0].msg[0], 30, UART_TIMEOUT);
		HAL_UART_Transmit(&huart1, (uint8_t *)&uart4RxMsg[0].msg[0], 30, UART_TIMEOUT);
#if 0
		if(retVal!=HAL_TIMEOUT) {
			for(i=0; i<30; i++) {
				printf("%02x ", uart4RxMsg[i]);
				if(i%0x10==0x0f) printf("\r\n");
			} printf("\r\n");
		}
#endif
#endif
}


int func_DebugConsoleReceive(ObcUartRxBufType *ubuf, uint8_t *state, QueueBufType *qbuf)
{
	uint8_t tmp;

	while(!func_ObcUartRxBuf_IsEmpty(ubuf))
	{
		tmp = func_ObcUartRxBuf_Getc(ubuf);
		dbgu_rx_low_recv(tmp);
		debug_rx_frame_handle();
	}
	return 0;
}

int func_ADCRead(ADC_HandleTypeDef *hadc)
{
	uint32_t ADC_Val[2]={0x0000,};
	int32_t temp1, temp2;
	double temp1f, temp2f;

//	for(uint8_t i=0;i<2;i++){
//		HAL_ADC_PollForConversion(hadc,100);
//		ADC_Val[i]=HAL_ADC_GetValue(hadc);
//	}

	HAL_ADC_Start(hadc);
	HAL_ADC_PollForConversion(hadc,100);
	ADC_Val[0]=HAL_ADC_GetValue(hadc);

	systeminfo.adc[0].value = ADC_Val[0] ;
	temp1f = func_Kalmanf( &ADCf[0], (double)(ADC_Val[0]));
	systeminfo.adc[0].kalman = (int)temp1f;
//	systeminfo.adc[0].celsius = (int) func_TemperatureRead(ADC_Val[0]);
	systeminfo.adc[0].celsius = (int) func_TemperatureRead((int)temp1f);


	HAL_ADC_Start(hadc);
	HAL_ADC_PollForConversion(hadc,100);

	ADC_Val[1]=HAL_ADC_GetValue(hadc);
	systeminfo.adc[1].value = ADC_Val[1] ;
	temp2f = func_Kalmanf(&ADCf[1], (double)(ADC_Val[1]));

	systeminfo.adc[1].kalman = (int)temp2f;
//	systeminfo.adc[1].celsius = (int)func_TemperatureRead(ADC_Val[1]);
	systeminfo.adc[1].celsius = (int) func_TemperatureRead((int)temp2f);


//	HAL_Delay(200);

//	adc_printf("adc0 ,%d, %d, %d, adc1, %d, %d, %d \r\n",
//			ADC_Val[0], (int)temp1,(int)temp1f,
//			ADC_Val[1], (int)temp2,(int)temp2f);

	return 1;
}

/* DMA Timeout event structure
 * Note: prevCNDTR initial value must be set to maximum size of DMA buffer!
*/

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
 	int cnt=0, delay=0;
	uint32_t lFromObcRxCnt = 1;
	uint32_t lFromRxModemRxCnt = 1;
	uint32_t lFromTxModemRxCnt = 1;

	uint16_t lObcRxBufCnt = 0;

	uint16_t lRxModemRxCnt = 0;
	uint16_t lTxModemRxCnt = 0;

	int32_t lObcKissDecStatus = 0;
	int32_t lRxModemKissDecStatus = 0;
	int32_t lTxModemKissDecStatus = 0;

	QueueBufType lObcRcvFrameBuf;

	QueueBufType lRxModemRcvFrameBuf;
	QueueBufType lTxModemRcvFrameBuf;

	QueueBufType lDebugeConsoleRcvBuf;

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();


  MX_DMA_Init();
  MX_USART3_UART_Init();
  MX_USART2_UART_Init();
  MX_USART1_UART_Init();
  MX_UART4_Init();
  MX_TIM3_Init();
  MX_ADC1_Init();


  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start_IT(&htim3);		//start timer interrupt
  debugInit();
  ShowLogo();
  func_KalmanInitAll();

  /* Start DMA */
   __HAL_UART_ENABLE_IT(&huart4, UART_IT_IDLE);
   __HAL_UART_ENABLE_IT(&huart4, UART_IT_RXNE);
   __HAL_UART_ENABLE_IT(&huart3, UART_IT_IDLE);
   __HAL_UART_ENABLE_IT(&huart3, UART_IT_RXNE);

  if(HAL_UART_Receive_DMA(&huart4, (uint8_t*)&dma_rx_buf[OBC_RX][0], DMA_BUF_SIZE) != HAL_OK)
  {
      Error_Handler();
  }

  if(HAL_UART_Receive_DMA(&huart3, (uint8_t*)&dma_rx_buf[RXMODEM_RX][0], DMA_BUF_SIZE) != HAL_OK)
  {
     Error_Handler();
  }

//  /* Disable Half Transfer Interrupt */
//  __HAL_DMA_DISABLE_IT(huart4.hdmarx, DMA_IT_HT);
//  __HAL_DMA_DISABLE_IT(huart3.hdmarx, DMA_IT_HT);

//	callsign_from_ascii();

  uart1RxBuf.Uid = ID_RXDEBUG_UART;

  uart2RxBuf.Uid = ID_RXMODEM_UART;
  uart3RxBuf.Uid = ID_TXMODEM_UART;
  uart4RxBuf.Uid = ID_OBC_UART;

  uart2TxBuf.Uid = ID_RXMODEM_UART;
  uart3TxBuf.Uid = ID_TXMODEM_UART;
  uart4TxBuf.Uid = ID_OBC_UART;

  lObcRcvFrameBuf.Fid = ID_FRAME_OBC;
  lTxModemRcvFrameBuf.Fid = ID_FRAME_TXMODEM;
  lRxModemRcvFrameBuf.Fid = ID_FRAME_RXMODEM;

  lDebugeConsoleRcvBuf.Fid = ID_DEBUG_CONSOLE;
  func_UartBuf_Clear(&uart1RxBuf);				// debug Serial Rx buffer

  func_UartBuf_Clear(&uart2RxBuf);				// Tx Modem receive buffer.
  func_UartBuf_Clear(&uart3RxBuf);				// Rx Modem receive buffer.
  func_ObcUartRxBuf_Clear(&uart4RxBuf);
  func_TxMdmUartTxBuf_Clear(&uart2TxBuf);
  func_UartBuf_Clear(&uart3TxBuf);
  func_UartBuf_Clear(&uart4TxBuf);

  //Enable UART Interrupt
//  HAL_UART_Receive_IT(&huart4, &uart4RxBuf.Data[uart4RxBuf.Head], 1);		//OBC - SOLINK SERIAL	 interrupt --> DMA
  HAL_UART_Receive_IT(&huart2, &uart2RxBuf.Data[uart2RxBuf.Head], 1);		//Tx Modem
//  HAL_UART_Receive_IT(&huart3, &uart3RxBuf.Data[uart3RxBuf.Head], 1);		//RX Modem				  interrupt --> DMA
  HAL_UART_Receive_IT(&huart1, &uart1RxBuf.Data[uart1RxBuf.Head], 1);		//Debug Serial. //kskim. 2021.11.10.

  func_QueueBuf_Clear(&lObcRcvFrameBuf);
  func_QueueBuf_Clear(&lRxModemRcvFrameBuf);
  func_QueueBuf_Clear(&lTxModemRcvFrameBuf);

  func_RfTxQueueBuf_Clear(&lToTxModemBuf);

  HAL_Delay(100);

  func_RfRxModem_Reset();
  func_RfTxModem_Reset();

  rfTxModemUartReady = 1;

  printf("Start SOLINK-UV Main MCU-STM32 Firmware \r\n");
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1)
	{
#if 0	//test thread
		test_function();
#else
#if 0	//modem rx
		//read uart3 msg from modem and send uart4 msg to obc
		Fn_RecvModemMsg();
		Fn_ProcessModemMsg();
		Fn_SendObcMsg();
//		HAL_UART_Transmit(&huart1, (uint8_t *)"4\r\n", 5, UART_TIMEOUT);
#endif
#if 0	//modem tx
		//read uart4 msg from obc and send uart2 msg to obc
//		HAL_UART_Transmit(&huart1, (uint8_t *)"5\r\n", 5, UART_TIMEOUT);
		Fn_RecvObcMsg();
//		HAL_UART_Transmit(&huart1, (uint8_t *)"6\r\n", 5, UART_TIMEOUT);
		Fn_ProcessObcMsg();
//		HAL_UART_Transmit(&huart1, (uint8_t *)"7\r\n", 5, UART_TIMEOUT);
		Fn_SendModemMsg();
//		HAL_UART_Transmit(&huart1, (uint8_t *)"8\r\n", 5, UART_TIMEOUT);
#endif
#endif

		/* OBC data Receive */
//		lObcKissDecStatus = func_DecKissDataFromObc(&uart4RxBuf, &recvObcKissState, &lObcRcvFrameBuf);		// ORIGIN

		lObcKissDecStatus = DMAStreamBufferRead(&huart4, &dma_uart_obc_rx, &recvObcKissState, &lObcRcvFrameBuf); //2021.12.13

#if 1
		if(lObcKissDecStatus > 0)
		{
			lObcRxBufCnt = func_QueueBuf_GetSize(&lObcRcvFrameBuf);

			if(lObcRxBufCnt > 0){
				func_SendToRfTxUart(lObcRcvFrameBuf.Data, lObcRcvFrameBuf.Length);
				func_QueueBuf_Clear(&lObcRcvFrameBuf);
			}
			else{
				continue;
			}


#if _DBG_RECV_DATA_
			for(int i = 0; i < lObcRxBufCnt; i++)
			{
				printf("%02X ", lObcRcvFrameBuf.Data[i]);
				if((i+1)%16 == 0) printf("\r\n");
			}
			printf("\r\n");
#endif


		}


#endif
		//Modem Rx data storage.
		lRxModemKissDecStatus = DMAStreamBufferRead(&huart3, &dma_uart_rxm_rx, &recvRxModemKissState, &lRxModemRcvFrameBuf); //2021.12.13

#if 1
		if(lRxModemKissDecStatus > 0)
		{
			int lBufCnt = func_QueueBuf_GetSize(&lRxModemRcvFrameBuf);

			printf("RX modem receive data count = %d \r\n",lBufCnt);
			if(lBufCnt > 0){

				func_Uart_Transmit(&huart4, &uart4TxBuf, lRxModemRcvFrameBuf.Data, lRxModemRcvFrameBuf.Length);
				func_QueueBuf_Clear(&lRxModemRcvFrameBuf);
			}
			else{
				continue;
			}


#if _DBG_RECV_DATA_
			for(int i = 0; i < cnt; i++)
			{
				printf("%02X, ", lRxModemRcvFrameBuf.Data[i]);
				if((i+1)%16 == 0) printf("\r\n");
			}
			printf(" %d \r\n",  cnt );
#endif
			printf("[%d]RxModem->Obc Transfer Size: %d (H:%d/T:%d) \r\n", lFromRxModemRxCnt++, lBufCnt, uart3RxBuf.Head, uart3RxBuf.Tail);
			//Modem Rx --> Modem TX bypass data.
//			func_Uart_Transmit(&huart4, &uart4TxBuf, &lRxModemRcvFrameBuf.Data[0], lRxModemRcvFrameBuf.Length);		//origin
//			func_Uart_Transmit(&huart2, &uart2TxBuf, &lRxModemRcvFrameBuf.Data[0], lRxModemRcvFrameBuf.Length);
//			func_TxMdmUart_Transmit(&huart2, &uart2TxBuf, &temp_Tx_modem, lRxModemRcvFrameBuf.Length - 9);

//			func_QueueBuf_Clear(&lRxModemRcvFrameBuf);
		}
#endif


#if _ENABLE_TX_MODEM_RECEIVE_
		//lTxModemKissDecStatus = func_BypassRawData(&uart2RxBuf, &lTxModemRcvFrameBuf);
		//lTxModemKissDecStatus = func_BypassKissData(&uart2RxBuf, &recvTxModemKissState, &lTxModemRcvFrameBuf);
		lTxModemKissDecStatus = func_DecKissData(&uart2RxBuf, &recvTxModemKissState, &lTxModemRcvFrameBuf);

#if 1
		if(lTxModemKissDecStatus > 0)
		{
			lTxModemRxCnt = func_QueueBuf_GetSize(&lTxModemRcvFrameBuf);
#if _DBG_RECV_DATA_
			for(int i = 0; i < cnt; i++)
			{
				printf("%02X ", lTxModemRcvFrameBuf.Data[i]);
				if((i+1)%16 == 0) printf("\r\n");
			}
			printf("\r\n");
#endif
			dbg_printf("[%d]TxModem->Obc Transfer Size: %d (H:%d/T:%d) \r\n", lFromTxModemRxCnt++, lTxModemRxCnt, uart2RxBuf.Head, uart2RxBuf.Tail);
			//func_Uart_Transmit(&huart4, &uart4TxBuf, &lTxModemRcvFrameBuf.Data[0], lTxModemRcvFrameBuf.Length);
			//HAL_UART_Transmit(&huart4, &lTxModemRcvFrameBuf.Data[0], lTxModemRcvFrameBuf.Length, 25000);
			func_FrameDecoding(&lTxModemRcvFrameBuf);
			func_QueueBuf_Clear(&lTxModemRcvFrameBuf);
		}
#endif
#endif

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
#if 0
		if(cnt%2==0) 	HAL_GPIO_WritePin(GPIO_1_GPIO_Port, GPIO_1_Pin|GPIO_2_Pin|GPIO_3_Pin, GPIO_PIN_RESET);
		else			HAL_GPIO_WritePin(GPIO_1_GPIO_Port, GPIO_1_Pin|GPIO_2_Pin|GPIO_3_Pin, GPIO_PIN_SET);
		cnt++;
#endif

		idle(&lDebugeConsoleRcvBuf);			//kaykim 2021.11.10
		func_ADCRead(&hadc1);
		HAL_Delay(1);
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 6;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_6) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_UART4;
  PeriphClkInitStruct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInitStruct.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Uart4ClockSelection = RCC_UART4CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* USART1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART1_IRQn);
  /* USART3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART3_IRQn);
  /* UART4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(UART4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(UART4_IRQn);
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = ENABLE;
  hadc1.Init.NbrOfDiscConversion = 1;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 2;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_14;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_144CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_15;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 159;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 5999;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief UART4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_UART4_Init(void)
{

  /* USER CODE BEGIN UART4_Init 0 */

  /* USER CODE END UART4_Init 0 */

  /* USER CODE BEGIN UART4_Init 1 */

  /* USER CODE END UART4_Init 1 */
  huart4.Instance = UART4;
  huart4.Init.BaudRate = 115200;
  huart4.Init.WordLength = UART_WORDLENGTH_8B;
  huart4.Init.StopBits = UART_STOPBITS_1;
  huart4.Init.Parity = UART_PARITY_NONE;
  huart4.Init.Mode = UART_MODE_TX_RX;
  huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart4.Init.OverSampling = UART_OVERSAMPLING_16;
  huart4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN UART4_Init 2 */

  /* USER CODE END UART4_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 9600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream1_IRQn);
  /* DMA1_Stream2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream2_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, CTRL_HALF1_Pin|CTRL_HALF2_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(SW_5V_HPA_GPIO_Port, SW_5V_HPA_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIO_3_GPIO_Port, GPIO_3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, RST_RFTX_N_Pin|RST_RFRX_N_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : CTRL_HALF1_Pin CTRL_HALF2_Pin SW_5V_HPA_Pin */
  GPIO_InitStruct.Pin = CTRL_HALF1_Pin|CTRL_HALF2_Pin|SW_5V_HPA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : ax_Pin */
  GPIO_InitStruct.Pin = ax_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ax_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PB1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : GPIO_3_Pin RST_RFTX_N_Pin RST_RFRX_N_Pin */
  GPIO_InitStruct.Pin = GPIO_3_Pin|RST_RFTX_N_Pin|RST_RFRX_N_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 4 */

uint16_t check_len(uint32_t *arrAddr)
{
	uint16_t i;
	uint8_t byteArray[MAX_BUFF_LEN] = {0,};

	memcpy(&byteArray[0], arrAddr, MAX_BUFF_LEN);

	for(i=0; i<MAX_BUFF_LEN; i++)
	{
		if (byteArray[i] == 0x00 && byteArray[i+1] == 0x00)
			return i;
		if (byteArray[i] == '\r' || byteArray[i+1] == '\n')	//null 2개면 길이 ?��?��
			return i;
//		else
//			continue;
	}
	return 0;
}

void callsign_from_ascii(void)
{
	uint8_t i;

	for(i=0; i<6; i++) {
		msg_sat_callsign_ascii[i] = (msg_sat_callsign[i] << 1);
		msg_gs_callsign_ascii[i] = (msg_gs_callsign[i] << 1);
	}
	msg_sat_callsign_ascii[6] = 0;
	msg_gs_callsign_ascii[6] = 0;
#if 0
  PRINT_LOG(LOG_LEVEL_MID, msg_sat_callsign, 10);
  PRINT_LOG(LOG_LEVEL_MID, msg_gs_callsign, 10);
  PRINT_LOG(LOG_LEVEL_MID, (char *)&msg_sat_callsign_ascii, 10);
  PRINT_LOG(LOG_LEVEL_MID, (char *)&msg_gs_callsign_ascii, 10);
#endif
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

	if(huart->Instance == UART4)		//OBC <--> SOLINK DATA LINE
	{
/* UART4 RECEIVE IT.
		if(uart4RxBuf.Head == (MAX_OBC_UART_RXBUF_SIZE-1)){
			uart4RxBuf.Head = 0;
		} else {
			uart4RxBuf.Head++;
		}

		HAL_UART_Receive_IT(&huart4, &uart4RxBuf.Data[uart4RxBuf.Head], 1);
*/


	}
	else if(huart->Instance == USART2)			// Modem serial.
	{
		if(uart2RxBuf.Head == (MAX_UART_SIZE-1))
			uart2RxBuf.Head = 0;
		else
			uart2RxBuf.Head++;

		HAL_UART_Receive_IT(&huart2, &uart2RxBuf.Data[uart2RxBuf.Head], 1);
	}

	else if(huart->Instance == USART3)			// Modem Rx Data.
	{
//		if(uart3RxBuf.Head == (MAX_UART_SIZE-1)){
//			uart3RxBuf.Head = 0;
//		} else {
//			uart3RxBuf.Head++;
//		}
//
//		HAL_UART_Receive_IT(&huart3, &uart3RxBuf.Data[uart3RxBuf.Head], 1);
	}


	else if(huart->Instance == USART1)			// Modem Rx Data.
	{
		if(uart1RxBuf.Head == (MAX_UART_SIZE-1)){
			uart1RxBuf.Head = 0;
		} else {
			uart1RxBuf.Head++;
		}

		HAL_UART_Receive_IT(&huart1, &uart1RxBuf.Data[uart1RxBuf.Head], 1);
	}

}

void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
{
}


void UART_IDLECallback(UART_HandleTypeDef *huart)
{


}


int32_t DMAStreamBufferRead(UART_HandleTypeDef *huart, DMA_Event_t *uart_rx, uint8_t *state, QueueBufType *qbuf)
{

	uint32_t rxPos;
	uint32_t i;

    int32_t rtl = 0;
    uint8_t c;


	rxPos = DMA_BUF_SIZE - __HAL_DMA_GET_COUNTER(huart->hdmarx);

    /* Timeout event */
//	if(dma_uart_rx.flag && (dma_uart_rx.prevCNDTR != rxPos))		//time out ?��벤트 발생?�� 버퍼?�� ?��?��?�� ?���? �??��.
	if(uart_rx->flag)
	{

		/* Determine new data length based on previous DMA_CNDTR value:
		 *  If previous CNDTR is less than DMA buffer size: there is old data in DMA buffer (from previous timeout) that has to be ignored.
		 *  If CNDTR == DMA buffer size: entire buffer content is new and has to be processed.
		*/
//		length = (dma_uart_rx.prevCNDTR < DMA_BUF_SIZE) ? (dma_uart_rx.prevCNDTR - currCNDTR) : (DMA_BUF_SIZE - currCNDTR);
//		dma_uart_rx.prevCNDTR = currCNDTR;
		uart_rx->flag = 0;
//		*state = uart_rx->flag;
		rtl = 1;
	}
	else                /* DMA Rx Complete event */
	{
//		length = DMA_BUF_SIZE - start;
//		dma_uart_rx.prevCNDTR = DMA_BUF_SIZE;
	}


	if(rxPos - uart_rx->prevCNDTR == 0)
	{
//			printf("uart_rx-> %d  no data \r\n",uart_rx->channel);
	} else{
//		printf("rxPos , %d, prevCNDTR, %d, \r\n", rxPos, uart_rx->prevCNDTR );
		//ToDo. processing data
		for(i = uart_rx->prevCNDTR ; i != rxPos; i = (i+1)%DMA_BUF_SIZE)
		{
			c = dma_rx_buf[uart_rx->channel][i];
			func_QueueBuf_Putc(qbuf, c);
		}
	}
	uart_rx->prevCNDTR = rxPos;


	return rtl;

}




uint32_t buf_pop(TxMdmUartTxBufType *p, int8_t* c, uint32_t bSize )
{
	if( p->Head == p->Tail ) return 0;
	*c = p->Data[ p->Tail ];
	p->Tail++;
	if( p->Tail >= bSize ) p->Tail = 0;
	return 1;
}


void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	uint16_t size = 0;
	uint8_t buf[MAX_UART_SIZE]={0,};
	int8_t c;

	if(huart->Instance == UART4)
	{
		uart4TxBuf.Tail += huart->TxXferSize;
		if(uart4TxBuf.Tail >= (MAX_UART_SIZE-1))
		{
			uart4TxBuf.Tail -= (MAX_UART_SIZE-1);
		}
		if(!func_UartBuf_IsEmpty(&uart4TxBuf))
		{
			if(uart4TxBuf.Head > uart4TxBuf.Tail)
			{
				size = uart4TxBuf.Head - uart4TxBuf.Tail;
			}
			else
			{
				size = MAX_UART_SIZE - uart4TxBuf.Tail;
			}
			HAL_UART_Transmit_IT(huart, &uart4TxBuf.Data[uart4TxBuf.Tail], size);
		}
	}

	else if(huart->Instance == USART2)
	{

		rfTxModemUartReady = 1;

#if 0				//kskim.
		if(!func_TxMdmUartTxBuf_IsEmpty(&uart2TxBuf))
		{
			while( buf_pop(&uart2TxBuf, &c, MAX_TXMDM_UART_TXBUF_SIZE ) && size < MAX_UART_SIZE)
			{
				buf[size] = c;
				size++;
			}
			HAL_UART_Transmit_IT(huart, buf, size);
			rfTxModemUartReady = 0;
		}

		//printf("H:%d/T:%d\r\n", uart2TxBuf.Head, uart2TxBuf.Tail);
#endif

#if 1
		uart2TxBuf.Tail += huart->TxXferSize;
		if(uart2TxBuf.Tail >= (MAX_TXMDM_UART_TXBUF_SIZE - 1) )
		{
			uart2TxBuf.Tail -= (MAX_TXMDM_UART_TXBUF_SIZE - 1);
		}

		if(!func_TxMdmUartTxBuf_IsEmpty(&uart2TxBuf))
		{

			if(uart2TxBuf.Head > uart2TxBuf.Tail)
			{
				size = uart2TxBuf.Head - uart2TxBuf.Tail;
			}
			else
			{
				size = (MAX_TXMDM_UART_TXBUF_SIZE) - uart2TxBuf.Tail;
			}

			HAL_UART_Transmit_IT(huart, &uart2TxBuf.Data[uart2TxBuf.Tail], size);
			rfTxModemUartReady = 0;
		}

		//printf("H:%d/T:%d\r\n", uart2TxBuf.Head, uart2TxBuf.Tail);
#endif
	}

	else if(huart->Instance == USART3)
	{
		uart3TxBuf.Tail += huart->TxXferSize;
		if(uart3TxBuf.Tail >= (MAX_UART_SIZE-1))
		{
			uart3TxBuf.Tail -= (MAX_UART_SIZE-1);
		}
		if(!func_UartBuf_IsEmpty(&uart3TxBuf))
		{
			if(uart3TxBuf.Head > uart3TxBuf.Tail)
			{
				size = uart3TxBuf.Head - uart3TxBuf.Tail;
			}
			else
			{
				size = MAX_UART_SIZE - uart3TxBuf.Tail;
			}
			HAL_UART_Transmit_IT(huart, &uart3TxBuf.Data[uart3TxBuf.Tail], size);
		}
	}

}

/*
 *
 * 	 console serial input processing
 *
 */
void idle(QueueBufType * DebugPortbuf)
{

	func_DebugConsoleReceive(&uart1RxBuf, &recvDebugConsoleState, DebugPortbuf);
	dbgu_tx_que_down(&huart1);			/*console send*/

	if( systeminfo.pps && mod.status )
	{
		systeminfo.pps = 0;
		ShowStatus();
	}
	if(systeminfo.ppm == 1){

		systeminfo.ppm = 0;
		adc_printf("> adc0,%d,t,%d,f,%d, adc1,%d,t,%d,f,%d, \r\n"
				,systeminfo.adc[0].value, systeminfo.adc[0].celsius, systeminfo.adc[0].kalman
				,systeminfo.adc[1].value, systeminfo.adc[1].celsius, systeminfo.adc[1].kalman
				);
	}

//	if(RxModemTimer.Flag == YES)
//	{
//		func_RfRxModem_Reset_manual(SET);
//	}



	/** To Do.
	 * show status.
	 * timeout check.
	 *
	 **/
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  /* Prevent unused argument(s) compilation warning */
//  UNUSED(GPIO_Pin);

  func_RfRxModem_Reset_manual(SET);
  rfTxModemTrasmitOK++;

  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_GPIO_EXTI_Callback could be implemented in the user file
   */
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
